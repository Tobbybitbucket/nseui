import { Component, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-review',
    templateUrl: './review.component.html',
})
export class ReviewComponent {
    submitting = false;
    disableForm = false;
    form = {
        field1: 'Lorem Ipsum',
        field2: 'Lorem Ipsum',
        field3: 'Lorem Ipsum',
        field4: 'Lorem Ipsum',

    };

    @Output() openPrev = new EventEmitter<any>();
    @Output() gotoFirstPage = new EventEmitter<any>();



    handlePrev() {
        this.openPrev.emit();
    }

    handleGotoFirstPage() {
        this.gotoFirstPage.emit();
    }


    toggleDisableForm() {
        this.disableForm = !this.disableForm;
    }
}

