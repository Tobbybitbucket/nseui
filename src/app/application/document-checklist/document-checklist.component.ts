import { Component, Output, EventEmitter, AfterViewInit, HostListener, ViewChild } from '@angular/core';
import { Table } from 'primeng/table';

@Component({
    selector: 'app-document-checklist',
    templateUrl: './document-checklist.component.html',
})
export class DocumentChecklistComponent implements AfterViewInit {
    submitting = false;
    currentSection = 'section2';
    screenHeight: any;
    screenWidth: any;
    @Output() openNext = new EventEmitter<any>();
    @Output() openPrev = new EventEmitter<any>();
    @ViewChild('dt') table: Table;
    selectedDocument: any;
    loading = false;
    documents = [
        {
            id: 1,
            sn: 1,
            name: 'CAC Document',
            document: 'DangoteCacdocument.pdf',
        },
        {
            id: 2,
            sn: 2,
            name: 'Document expected 2',
            document: '',
        },
        {
            id: 3,
            sn: 3,
            name: 'Document expected 3',
            document: '',
        },
        {
            id: 4,
            sn: 4,
            name: 'Document expected 4',
            document: '',
        },
        {
            id: 5,
            sn: 5,
            name: 'Document expected 5',
            document: 'DangoteCacdocument.pdf',
        },
        {
            id: 6,
            sn: 6,
            name: 'Document expected 6',
            document: '',
        },
        {
            id: 7,
            sn: 7,
            name: 'Document expected 7',
            document: '',
        },
        {
            id: 8,
            sn: 8,
            name: 'Document expected 8',
            document: '',
        }
    ];

    buttonItems = [
        {
            label: 'Upload', command: () => {
                console.log('assign clicked');
            }
        },
        {
            label: 'Delete', command: () => {
                console.log('action x clicked');
            }
        }
    ];

    cols = [
        { field: 'sn', header: 'S/N' },
        { field: 'name', header: 'Expected Document Name' },
        { field: 'document', header: 'Document Uploaded' },
        { field: 'action', header: '' }
    ];

    constructor() {
        this.onResize();
    }

    @HostListener('window:resize', ['$event'])
    onResize(event?) {
        this.screenHeight = window.innerHeight;
        this.screenWidth = window.innerWidth;
    }


    ngAfterViewInit() {
    }

    handleOpenNext() {
        this.openNext.emit();
    }

    handlePrev() {
        this.openPrev.emit();
    }

    setSelectedDocument(document): void {
        this.selectedDocument = document;
    }

    onSectionChange(sectionId: string) {
        this.currentSection = sectionId;
    }

    scrollTo(section) {
        document.querySelector('#' + section)
            .scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
    }

}

