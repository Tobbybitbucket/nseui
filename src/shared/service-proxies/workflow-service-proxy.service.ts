import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Observable, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { Injectable, Inject, Optional, InjectionToken } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';
import { TokenService } from 'abp-ng2-module';

import * as moment from 'moment';

export const API_BASE_URL = new InjectionToken<string>('API_BASE_URL');


@Injectable({
  providedIn: 'root'
})
export class WorkflowServiceProxyService {

  constructor() { }
}



@Injectable()
export class ApplicationLetterServiceProxy {
    private http: HttpClient;
    private baseUrl: string;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

    constructor(private _tokenService: TokenService,
      @Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
        this.http = http;
        this.baseUrl = baseUrl ? baseUrl : "";
    }

    /**
     * @param applicationId (optional) 
     * @return Success
     */
    getLetter(applicationId: number | undefined): Observable<ApprovalLogDocDTOIListApiResult> {
        let url_ = this.baseUrl + "/api/ApplicationLetter/GetLetter?";
        if (applicationId === null)
            throw new Error("The parameter 'applicationId' cannot be null.");
        else if (applicationId !== undefined)
            url_ += "applicationId=" + encodeURIComponent("" + applicationId) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processGetLetter(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetLetter(<any>response_);
                } catch (e) {
                    return <Observable<ApprovalLogDocDTOIListApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<ApprovalLogDocDTOIListApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processGetLetter(response: HttpResponseBase): Observable<ApprovalLogDocDTOIListApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = ApprovalLogDocDTOIListApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ApprovalLogDocDTOIListApiResult>(<any>null);
    }

    /**
     * @param body (optional) 
     * @return Success
     */
    saveLetter(body: ApprovalLogDoc | undefined): Observable<MessageOutApiResult> {
        let url_ = this.baseUrl + "/api/ApplicationLetter/SaveLetter";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_ : any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processSaveLetter(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processSaveLetter(<any>response_);
                } catch (e) {
                    return <Observable<MessageOutApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<MessageOutApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processSaveLetter(response: HttpResponseBase): Observable<MessageOutApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = MessageOutApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<MessageOutApiResult>(<any>null);
    }
}

@Injectable()
export class ApprovalLogDocServiceProxy {
    private http: HttpClient;
    private baseUrl: string;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

    constructor(private _tokenService: TokenService,
@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
        this.http = http;
        this.baseUrl = baseUrl ? baseUrl : "";
    }

    /**
     * @param applicationId (optional) 
     * @return Success
     */
    getDocs(applicationId: number | undefined): Observable<ApprovalLogDocDTOIListApiResult> {
        let url_ = this.baseUrl + "/api/ApprovalLogDoc/GetDocs?";
        if (applicationId === null)
            throw new Error("The parameter 'applicationId' cannot be null.");
        else if (applicationId !== undefined)
            url_ += "applicationId=" + encodeURIComponent("" + applicationId) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processGetDocs(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetDocs(<any>response_);
                } catch (e) {
                    return <Observable<ApprovalLogDocDTOIListApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<ApprovalLogDocDTOIListApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processGetDocs(response: HttpResponseBase): Observable<ApprovalLogDocDTOIListApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = ApprovalLogDocDTOIListApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ApprovalLogDocDTOIListApiResult>(<any>null);
    }

    /**
     * @param body (optional) 
     * @return Success
     */
    saveDoc(body: ApprovalLogDoc | undefined): Observable<MessageOutApiResult> {
        let url_ = this.baseUrl + "/api/ApprovalLogDoc/SaveDoc";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_ : any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processSaveDoc(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processSaveDoc(<any>response_);
                } catch (e) {
                    return <Observable<MessageOutApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<MessageOutApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processSaveDoc(response: HttpResponseBase): Observable<MessageOutApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = MessageOutApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<MessageOutApiResult>(<any>null);
    }
}

@Injectable()
export class ApprovalLogMessageServiceProxy {
    private http: HttpClient;
    private baseUrl: string;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

    constructor(private _tokenService: TokenService,
@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
        this.http = http;
        this.baseUrl = baseUrl ? baseUrl : "";
    }

    /**
     * @param applicationId (optional) 
     * @return Success
     */
    getReviewMessages(applicationId: number | undefined): Observable<AppLogMessageDTOIListApiResult> {
        let url_ = this.baseUrl + "/api/ApprovalLogMessage/GetReviewMessages?";
        if (applicationId === null)
            throw new Error("The parameter 'applicationId' cannot be null.");
        else if (applicationId !== undefined)
            url_ += "applicationId=" + encodeURIComponent("" + applicationId) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processGetReviewMessages(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetReviewMessages(<any>response_);
                } catch (e) {
                    return <Observable<AppLogMessageDTOIListApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<AppLogMessageDTOIListApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processGetReviewMessages(response: HttpResponseBase): Observable<AppLogMessageDTOIListApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = AppLogMessageDTOIListApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<AppLogMessageDTOIListApiResult>(<any>null);
    }

    /**
     * @param body (optional) 
     * @return Success
     */
    saveReviewMessages(body: ApprovalLogMessage | undefined): Observable<MessageOutApiResult> {
        let url_ = this.baseUrl + "/api/ApprovalLogMessage/SaveReviewMessages";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_ : any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processSaveReviewMessages(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processSaveReviewMessages(<any>response_);
                } catch (e) {
                    return <Observable<MessageOutApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<MessageOutApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processSaveReviewMessages(response: HttpResponseBase): Observable<MessageOutApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = MessageOutApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<MessageOutApiResult>(<any>null);
    }
}

@Injectable()
export class ApprovalReviewServiceProxy {
    private http: HttpClient;
    private baseUrl: string;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

    constructor(private _tokenService: TokenService,
@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
        this.http = http;
        this.baseUrl = baseUrl ? baseUrl : "";
    }

    /**
     * @param alogid (optional) 
     * @param userid (optional) 
     * @return Success
     */
    hasAuthorithy(alogid: number | undefined, userid: number | undefined): Observable<BooleanApiResult> {
        let url_ = this.baseUrl + "/api/ApprovalReview/HasAuthorithy?";
        if (alogid === null)
            throw new Error("The parameter 'alogid' cannot be null.");
        else if (alogid !== undefined)
            url_ += "alogid=" + encodeURIComponent("" + alogid) + "&";
        if (userid === null)
            throw new Error("The parameter 'userid' cannot be null.");
        else if (userid !== undefined)
            url_ += "userid=" + encodeURIComponent("" + userid) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processHasAuthorithy(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processHasAuthorithy(<any>response_);
                } catch (e) {
                    return <Observable<BooleanApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<BooleanApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processHasAuthorithy(response: HttpResponseBase): Observable<BooleanApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = BooleanApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<BooleanApiResult>(<any>null);
    }

    postApprovalLog(body: ApprovalLog | undefined): Observable<ApprovalResponseDTOApiResult> {
        let url_ = this.baseUrl + "/api/ApprovalReview/PostApprovalLog";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_ : any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain"
            })
        };

        return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processPostApprovalLog(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processPostApprovalLog(<any>response_);
                } catch (e) {
                    return <Observable<ApprovalResponseDTOApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<ApprovalResponseDTOApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processPostApprovalLog(response: HttpResponseBase): Observable<ApprovalResponseDTOApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = ApprovalResponseDTOApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ApprovalResponseDTOApiResult>(<any>null);
    }

     /**
     * @param alogid (optional) 
     * @return Success
     */
    getApprovalLog(alogid: number | undefined): Observable<ApprovalLogApiResult> {
        let url_ = this.baseUrl + "/api/ApprovalReview/GetApprovalLog?";
        if (alogid === null)
            throw new Error("The parameter 'alogid' cannot be null.");
        else if (alogid !== undefined)
            url_ += "alogid=" + encodeURIComponent("" + alogid) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain"
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processGetApprovalLog(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetApprovalLog(<any>response_);
                } catch (e) {
                    return <Observable<ApprovalLogApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<ApprovalLogApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processGetApprovalLog(response: HttpResponseBase): Observable<ApprovalLogApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = ApprovalLogApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ApprovalLogApiResult>(<any>null);
    }

    /**
     * @param processId (optional) 
     * @param itemId (optional) 
     * @return Success
     */
    startApprovalWorkflow(processId: number | undefined, itemId: number | undefined): Observable<ApprovalResponseDTOApiResult> {
        let url_ = this.baseUrl + "/api/ApprovalReview/StartApprovalWorkflow?";
        if (processId === null)
            throw new Error("The parameter 'processId' cannot be null.");
        else if (processId !== undefined)
            url_ += "processId=" + encodeURIComponent("" + processId) + "&";
        if (itemId === null)
            throw new Error("The parameter 'itemId' cannot be null.");
        else if (itemId !== undefined)
            url_ += "itemId=" + encodeURIComponent("" + itemId) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processStartApprovalWorkflow(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processStartApprovalWorkflow(<any>response_);
                } catch (e) {
                    return <Observable<ApprovalResponseDTOApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<ApprovalResponseDTOApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processStartApprovalWorkflow(response: HttpResponseBase): Observable<ApprovalResponseDTOApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = ApprovalResponseDTOApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ApprovalResponseDTOApiResult>(<any>null);
    }

    /**
     * @param itemId (optional) 
     * @return Success
     */
    fetchItemApprovalLog(itemId: number | undefined): Observable<ApprovalLogDTOIListApiResult> {
        let url_ = this.baseUrl + "/api/ApprovalReview/FetchItemApprovalLog?";
        if (itemId === null)
            throw new Error("The parameter 'itemId' cannot be null.");
        else if (itemId !== undefined)
            url_ += "itemId=" + encodeURIComponent("" + itemId) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processFetchItemApprovalLog(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processFetchItemApprovalLog(<any>response_);
                } catch (e) {
                    return <Observable<ApprovalLogDTOIListApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<ApprovalLogDTOIListApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processFetchItemApprovalLog(response: HttpResponseBase): Observable<ApprovalLogDTOIListApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = ApprovalLogDTOIListApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ApprovalLogDTOIListApiResult>(<any>null);
    }

    /**
     * @param processId (optional) 
     * @param userId (optional) 
     * @return Success
     */
    fetchMyPendingApprovalLog(processId: number | undefined, userId: number | undefined): Observable<ApprovalLogDTOIListApiResult> {
        let url_ = this.baseUrl + "/api/ApprovalReview/FetchMyPendingApprovalLog?";
        if (processId === null)
            throw new Error("The parameter 'processId' cannot be null.");
        else if (processId !== undefined)
            url_ += "processId=" + encodeURIComponent("" + processId) + "&";
        if (userId === null)
            throw new Error("The parameter 'userId' cannot be null.");
        else if (userId !== undefined)
            url_ += "userId=" + encodeURIComponent("" + userId) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processFetchMyPendingApprovalLog(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processFetchMyPendingApprovalLog(<any>response_);
                } catch (e) {
                    return <Observable<ApprovalLogDTOIListApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<ApprovalLogDTOIListApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processFetchMyPendingApprovalLog(response: HttpResponseBase): Observable<ApprovalLogDTOIListApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = ApprovalLogDTOIListApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ApprovalLogDTOIListApiResult>(<any>null);
    }

    /**
     * @param processId (optional) 
     * @param userId (optional) 
     * @param fetchType (optional) 
     * @param startDate (optional) 
     * @param endDate (optional) 
     * @param flag (optional) 
     * @return Success
     */
    countApprovalLog(processId: number | undefined, userId: number | undefined, fetchType: number | null | undefined, startDate: moment.Moment | null | undefined, endDate: moment.Moment | null | undefined, flag: string | null | undefined): Observable<Int32ApiResult> {
        let url_ = this.baseUrl + "/api/ApprovalReview/CountApprovalLog?";
        if (processId === null)
            throw new Error("The parameter 'processId' cannot be null.");
        else if (processId !== undefined)
            url_ += "processId=" + encodeURIComponent("" + processId) + "&";
        if (userId === null)
            throw new Error("The parameter 'userId' cannot be null.");
        else if (userId !== undefined)
            url_ += "userId=" + encodeURIComponent("" + userId) + "&";
        if (fetchType !== undefined && fetchType !== null)
            url_ += "fetchType=" + encodeURIComponent("" + fetchType) + "&";
        if (startDate !== undefined && startDate !== null)
            url_ += "startDate=" + encodeURIComponent(startDate ? "" + startDate.toJSON() : "") + "&";
        if (endDate !== undefined && endDate !== null)
            url_ += "endDate=" + encodeURIComponent(endDate ? "" + endDate.toJSON() : "") + "&";
        if (flag !== undefined && flag !== null)
            url_ += "flag=" + encodeURIComponent("" + flag) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processCountApprovalLog(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processCountApprovalLog(<any>response_);
                } catch (e) {
                    return <Observable<Int32ApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<Int32ApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processCountApprovalLog(response: HttpResponseBase): Observable<Int32ApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = Int32ApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<Int32ApiResult>(<any>null);
    }
}

@Injectable()
export class ApprovalSetupServiceProxy {
    private http: HttpClient;
    private baseUrl: string;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

    constructor(private _tokenService: TokenService,
@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
        this.http = http;
        this.baseUrl = baseUrl ? baseUrl : "";
    }

    /**
     * @return Success
     */
    getFormSections(): Observable<FormSectionIListApiResult> {
        let url_ = this.baseUrl + "/api/ApprovalSetup/GetFormSections";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processGetFormSections(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetFormSections(<any>response_);
                } catch (e) {
                    return <Observable<FormSectionIListApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<FormSectionIListApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processGetFormSections(response: HttpResponseBase): Observable<FormSectionIListApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = FormSectionIListApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<FormSectionIListApiResult>(<any>null);
    }

    /**
     * @param body (optional) 
     * @return Success
     */
    saveFormSection(body: FormSection | undefined): Observable<MessageOutApiResult> {
        let url_ = this.baseUrl + "/api/ApprovalSetup/SaveFormSection";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_ : any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processSaveFormSection(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processSaveFormSection(<any>response_);
                } catch (e) {
                    return <Observable<MessageOutApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<MessageOutApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processSaveFormSection(response: HttpResponseBase): Observable<MessageOutApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = MessageOutApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<MessageOutApiResult>(<any>null);
    }

    /**
     * @param id (optional) 
     * @return Success
     */
    getApprovalProcesses(id: number | undefined): Observable<ApprovalProcessIListApiResult> {
        let url_ = this.baseUrl + "/api/ApprovalSetup/GetApprovalProcesses?";
        if (id === null)
            throw new Error("The parameter 'id' cannot be null.");
        else if (id !== undefined)
            url_ += "id=" + encodeURIComponent("" + id) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processGetApprovalProcesses(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetApprovalProcesses(<any>response_);
                } catch (e) {
                    return <Observable<ApprovalProcessIListApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<ApprovalProcessIListApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processGetApprovalProcesses(response: HttpResponseBase): Observable<ApprovalProcessIListApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = ApprovalProcessIListApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ApprovalProcessIListApiResult>(<any>null);
    }

    /**
     * @param approvalProcessId (optional) 
     * @param approvalStepId (optional) 
     * @return Success
     */
    getApprovalSteps(approvalProcessId: number | undefined, approvalStepId: number | undefined): Observable<ApprovalStepIListApiResult> {
        let url_ = this.baseUrl + "/api/ApprovalSetup/GetApprovalSteps?";
        if (approvalProcessId === null)
            throw new Error("The parameter 'approvalProcessId' cannot be null.");
        else if (approvalProcessId !== undefined)
            url_ += "approvalProcessId=" + encodeURIComponent("" + approvalProcessId) + "&";
        if (approvalStepId === null)
            throw new Error("The parameter 'approvalStepId' cannot be null.");
        else if (approvalStepId !== undefined)
            url_ += "approvalStepId=" + encodeURIComponent("" + approvalStepId) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processGetApprovalSteps(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetApprovalSteps(<any>response_);
                } catch (e) {
                    return <Observable<ApprovalStepIListApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<ApprovalStepIListApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processGetApprovalSteps(response: HttpResponseBase): Observable<ApprovalStepIListApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = ApprovalStepIListApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ApprovalStepIListApiResult>(<any>null);
    }

    /**
     * @param approvalProcessId (optional) 
     * @param approvalStepId (optional) 
     * @return Success
     */
    deleteApprovalStep(approvalProcessId: number | undefined, approvalStepId: number | undefined): Observable<MessageOutApiResult> {
        let url_ = this.baseUrl + "/api/ApprovalSetup/DeleteApprovalStep?";
        if (approvalProcessId === null)
            throw new Error("The parameter 'approvalProcessId' cannot be null.");
        else if (approvalProcessId !== undefined)
            url_ += "approvalProcessId=" + encodeURIComponent("" + approvalProcessId) + "&";
        if (approvalStepId === null)
            throw new Error("The parameter 'approvalStepId' cannot be null.");
        else if (approvalStepId !== undefined)
            url_ += "approvalStepId=" + encodeURIComponent("" + approvalStepId) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processDeleteApprovalStep(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processDeleteApprovalStep(<any>response_);
                } catch (e) {
                    return <Observable<MessageOutApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<MessageOutApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processDeleteApprovalStep(response: HttpResponseBase): Observable<MessageOutApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = MessageOutApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<MessageOutApiResult>(<any>null);
    }

    /**
     * @param body (optional) 
     * @return Success
     */
    saveApprovalProcesses(body: ApprovalProcess | undefined): Observable<MessageOutApiResult> {
        let url_ = this.baseUrl + "/api/ApprovalSetup/SaveApprovalProcesses";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_ : any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processSaveApprovalProcesses(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processSaveApprovalProcesses(<any>response_);
                } catch (e) {
                    return <Observable<MessageOutApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<MessageOutApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processSaveApprovalProcesses(response: HttpResponseBase): Observable<MessageOutApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = MessageOutApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<MessageOutApiResult>(<any>null);
    }

    /**
     * @param body (optional) 
     * @return Success
     */
    saveApprovalStep(body: ApprovalStep | undefined): Observable<MessageOutApiResult> {
        let url_ = this.baseUrl + "/api/ApprovalSetup/SaveApprovalStep";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_ : any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processSaveApprovalStep(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processSaveApprovalStep(<any>response_);
                } catch (e) {
                    return <Observable<MessageOutApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<MessageOutApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processSaveApprovalStep(response: HttpResponseBase): Observable<MessageOutApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = MessageOutApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<MessageOutApiResult>(<any>null);
    }
}



@Injectable()
export class DocTemplateServiceProxy {
    private http: HttpClient;
    private baseUrl: string;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

    constructor(private _tokenService: TokenService,
@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
        this.http = http;
        this.baseUrl = baseUrl ? baseUrl : "";
    }

    /**
     * @return Success
     */
    getDocTemplateList(): Observable<DocTemplateIListApiResult> {
        let url_ = this.baseUrl + "/api/DocTemplate/GetDocTemplateList";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processGetDocTemplateList(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetDocTemplateList(<any>response_);
                } catch (e) {
                    return <Observable<DocTemplateIListApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<DocTemplateIListApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processGetDocTemplateList(response: HttpResponseBase): Observable<DocTemplateIListApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = DocTemplateIListApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<DocTemplateIListApiResult>(<any>null);
    }

    /**
     * @param name (optional) 
     * @param id (optional) 
     * @return Success
     */
    getDocTemplate(name: string | null | undefined, id: number | undefined): Observable<DocTemplateApiResult> {
        let url_ = this.baseUrl + "/api/DocTemplate/GetDocTemplate?";
        if (name !== undefined && name !== null)
            url_ += "name=" + encodeURIComponent("" + name) + "&";
        if (id === null)
            throw new Error("The parameter 'id' cannot be null.");
        else if (id !== undefined)
            url_ += "id=" + encodeURIComponent("" + id) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processGetDocTemplate(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetDocTemplate(<any>response_);
                } catch (e) {
                    return <Observable<DocTemplateApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<DocTemplateApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processGetDocTemplate(response: HttpResponseBase): Observable<DocTemplateApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = DocTemplateApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<DocTemplateApiResult>(<any>null);
    }

    /**
     * @param body (optional) 
     * @return Success
     */
    saveDocTemplate(body: DocTemplate | undefined): Observable<MessageOutApiResult> {
        let url_ = this.baseUrl + "/api/DocTemplate/SaveDocTemplate";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_ : any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain",
 'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processSaveDocTemplate(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processSaveDocTemplate(<any>response_);
                } catch (e) {
                    return <Observable<MessageOutApiResult>><any>_observableThrow(e);
                }
            } else
                return <Observable<MessageOutApiResult>><any>_observableThrow(response_);
        }));
    }

    protected processSaveDocTemplate(response: HttpResponseBase): Observable<MessageOutApiResult> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = MessageOutApiResult.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result400: any = null;
            let resultData400 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            if (resultData400) {
                result400 = {} as any;
                for (let key in resultData400) {
                    if (resultData400.hasOwnProperty(key))
                        result400[key] = resultData400[key];
                }
            }
            return throwException("Bad Request", status, _responseText, _headers, result400);
            }));
        } else if (status === 500) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("Server Error", status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<MessageOutApiResult>(<any>null);
    }
}



export class ApiException extends Error {
  message: string;
  status: number;
  response: string;
  headers: { [key: string]: any; };
  result: any;

  constructor(message: string, status: number, response: string, headers: { [key: string]: any; }, result: any) {
      super();

      this.message = message;
      this.status = status;
      this.response = response;
      this.headers = headers;
      this.result = result;
  }

  protected isApiException = true;

  static isApiException(obj: any): obj is ApiException {
      return obj.isApiException === true;
  }
}

function throwException(message: string, status: number, response: string, headers: { [key: string]: any; }, result?: any): Observable<any> {
  if (result !== null && result !== undefined)
      return _observableThrow(result);
  else
      return _observableThrow(new ApiException(message, status, response, headers, null));
}

function blobToText(blob: any): Observable<string> {
  return new Observable<string>((observer: any) => {
      if (!blob) {
          observer.next("");
          observer.complete();
      } else {
          let reader = new FileReader();
          reader.onload = event => {
              observer.next((<any>event.target).result);
              observer.complete();
          };
          reader.readAsText(blob);
      }
  });
}


export class ApprovalLogDocDTO implements IApprovalLogDocDTO {
  id: number;
  approvalLogId: number;
  uploadedBy: string | undefined;
  fileName: string | undefined;
  fileTitle: string | undefined;
  filePath: string | undefined;
  dateCreated: moment.Moment;

  constructor(data?: IApprovalLogDocDTO) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.id = _data["id"];
          this.approvalLogId = _data["approvalLogId"];
          this.uploadedBy = _data["uploadedBy"];
          this.fileName = _data["fileName"];
          this.fileTitle = _data["fileTitle"];
          this.filePath = _data["filePath"];
          this.dateCreated = _data["dateCreated"] ? moment(_data["dateCreated"].toString()) : <any>undefined;
      }
  }

  static fromJS(data: any): ApprovalLogDocDTO {
      data = typeof data === 'object' ? data : {};
      let result = new ApprovalLogDocDTO();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["id"] = this.id;
      data["approvalLogId"] = this.approvalLogId;
      data["uploadedBy"] = this.uploadedBy;
      data["fileName"] = this.fileName;
      data["fileTitle"] = this.fileTitle;
      data["filePath"] = this.filePath;
      data["dateCreated"] = this.dateCreated ? this.dateCreated.toISOString() : <any>undefined;
      return data; 
  }

  clone(): ApprovalLogDocDTO {
      const json = this.toJSON();
      let result = new ApprovalLogDocDTO();
      result.init(json);
      return result;
  }
}

export interface IApprovalLogDocDTO {
  id: number;
  approvalLogId: number;
  uploadedBy: string | undefined;
  fileName: string | undefined;
  fileTitle: string | undefined;
  filePath: string | undefined;
  dateCreated: moment.Moment;
}

export class ApprovalLogDocDTOIListApiResult implements IApprovalLogDocDTOIListApiResult {
  hasError: boolean;
  message: string | undefined;
  result: ApprovalLogDocDTO[] | undefined;

  constructor(data?: IApprovalLogDocDTOIListApiResult) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.hasError = _data["hasError"];
          this.message = _data["message"];
          if (Array.isArray(_data["result"])) {
              this.result = [] as any;
              for (let item of _data["result"])
                  this.result.push(ApprovalLogDocDTO.fromJS(item));
          }
      }
  }

  static fromJS(data: any): ApprovalLogDocDTOIListApiResult {
      data = typeof data === 'object' ? data : {};
      let result = new ApprovalLogDocDTOIListApiResult();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["hasError"] = this.hasError;
      data["message"] = this.message;
      if (Array.isArray(this.result)) {
          data["result"] = [];
          for (let item of this.result)
              data["result"].push(item.toJSON());
      }
      return data; 
  }

  clone(): ApprovalLogDocDTOIListApiResult {
      const json = this.toJSON();
      let result = new ApprovalLogDocDTOIListApiResult();
      result.init(json);
      return result;
  }
}

export interface IApprovalLogDocDTOIListApiResult {
  hasError: boolean;
  message: string | undefined;
  result: ApprovalLogDocDTO[] | undefined;
}

export class ApprovalLogMessage implements IApprovalLogMessage {
  postedById: number;
  approvalLogId: number;
  message: string | undefined;
  formSectionId: number;
  dateCreated: moment.Moment;
  approvalLog: ApprovalLog;
  id: number;

  constructor(data?: IApprovalLogMessage) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.postedById = _data["postedById"];
          this.approvalLogId = _data["approvalLogId"];
          this.message = _data["message"];
          this.formSectionId = _data["formSectionId"];
          this.dateCreated = _data["dateCreated"] ? moment(_data["dateCreated"].toString()) : <any>undefined;
          this.approvalLog = _data["approvalLog"] ? ApprovalLog.fromJS(_data["approvalLog"]) : <any>undefined;
          this.id = _data["id"];
      }
  }

  static fromJS(data: any): ApprovalLogMessage {
      data = typeof data === 'object' ? data : {};
      let result = new ApprovalLogMessage();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["postedById"] = this.postedById;
      data["approvalLogId"] = this.approvalLogId;
      data["message"] = this.message;
      data["formSectionId"] = this.formSectionId;
      data["dateCreated"] = this.dateCreated ? this.dateCreated.toISOString() : <any>undefined;
      data["approvalLog"] = this.approvalLog ? this.approvalLog.toJSON() : <any>undefined;
      data["id"] = this.id;
      return data; 
  }

  clone(): ApprovalLogMessage {
      const json = this.toJSON();
      let result = new ApprovalLogMessage();
      result.init(json);
      return result;
  }
}

export interface IApprovalLogMessage {
  postedById: number;
  approvalLogId: number;
  message: string | undefined;
  formSectionId: number;
  dateCreated: moment.Moment;
  approvalLog: ApprovalLog;
  id: number;
}

export class ApprovalLog implements IApprovalLog {
  itemId: number;
  approvalProcessId: number;
  approvalStepId: number;
  stepSn: number;
  roleId: number;
  userId: number;
  notifyDate: moment.Moment | undefined;
  flag: string | undefined;
  reviewedBy: number;
  reviewStatus: number | undefined;
  reviewDate: moment.Moment | undefined;
  approvalLogDocs: ApprovalLogDoc[] | undefined;
  approvalLogMessages: ApprovalLogMessage[] | undefined;
  id: number;

  constructor(data?: IApprovalLog) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.itemId = _data["itemId"];
          this.approvalProcessId = _data["approvalProcessId"];
          this.approvalStepId = _data["approvalStepId"];
          this.stepSn = _data["stepSn"];
          this.roleId = _data["roleId"];
          this.userId = _data["userId"];
          this.notifyDate = _data["notifyDate"] ? moment(_data["notifyDate"].toString()) : <any>undefined;
          this.flag = _data["flag"];
          this.reviewedBy = _data["reviewedBy"];
          this.reviewStatus = _data["reviewStatus"];
          this.reviewDate = _data["reviewDate"] ? moment(_data["reviewDate"].toString()) : <any>undefined;
          if (Array.isArray(_data["approvalLogDocs"])) {
              this.approvalLogDocs = [] as any;
              for (let item of _data["approvalLogDocs"])
                  this.approvalLogDocs.push(ApprovalLogDoc.fromJS(item));
          }
          if (Array.isArray(_data["approvalLogMessages"])) {
              this.approvalLogMessages = [] as any;
              for (let item of _data["approvalLogMessages"])
                  this.approvalLogMessages.push(ApprovalLogMessage.fromJS(item));
          }
          this.id = _data["id"];
      }
  }

  static fromJS(data: any): ApprovalLog {
      data = typeof data === 'object' ? data : {};
      let result = new ApprovalLog();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["itemId"] = this.itemId;
      data["approvalProcessId"] = this.approvalProcessId;
      data["approvalStepId"] = this.approvalStepId;
      data["stepSn"] = this.stepSn;
      data["roleId"] = this.roleId;
      data["userId"] = this.userId;
      data["notifyDate"] = this.notifyDate ? this.notifyDate.toISOString() : <any>undefined;
      data["flag"] = this.flag;
      data["reviewedBy"] = this.reviewedBy;
      data["reviewStatus"] = this.reviewStatus;
      data["reviewDate"] = this.reviewDate ? this.reviewDate.toISOString() : <any>undefined;
      if (Array.isArray(this.approvalLogDocs)) {
          data["approvalLogDocs"] = [];
          for (let item of this.approvalLogDocs)
              data["approvalLogDocs"].push(item.toJSON());
      }
      if (Array.isArray(this.approvalLogMessages)) {
          data["approvalLogMessages"] = [];
          for (let item of this.approvalLogMessages)
              data["approvalLogMessages"].push(item.toJSON());
      }
      data["id"] = this.id;
      return data; 
  }

  clone(): ApprovalLog {
      const json = this.toJSON();
      let result = new ApprovalLog();
      result.init(json);
      return result;
  }
}

export interface IApprovalLog {
  itemId: number;
  approvalProcessId: number;
  approvalStepId: number;
  stepSn: number;
  roleId: number;
  userId: number;
  notifyDate: moment.Moment | undefined;
  flag: string | undefined;
  reviewedBy: number;
  reviewStatus: number | undefined;
  reviewDate: moment.Moment | undefined;
  approvalLogDocs: ApprovalLogDoc[] | undefined;
  approvalLogMessages: ApprovalLogMessage[] | undefined;
  id: number;
}

export class ApprovalLogDoc implements IApprovalLogDoc {
  approvalLogId: number;
  uploadedById: number;
  fileName: string | undefined;
  fileTitle: string | undefined;
  filePath: string | undefined;
  dateCreated: moment.Moment;
  approvalLog: ApprovalLog;
  id: number;

  constructor(data?: IApprovalLogDoc) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.approvalLogId = _data["approvalLogId"];
          this.uploadedById = _data["uploadedById"];
          this.fileName = _data["fileName"];
          this.fileTitle = _data["fileTitle"];
          this.filePath = _data["filePath"];
          this.dateCreated = _data["dateCreated"] ? moment(_data["dateCreated"].toString()) : <any>undefined;
          this.approvalLog = _data["approvalLog"] ? ApprovalLog.fromJS(_data["approvalLog"]) : <any>undefined;
          this.id = _data["id"];
      }
  }

  static fromJS(data: any): ApprovalLogDoc {
      data = typeof data === 'object' ? data : {};
      let result = new ApprovalLogDoc();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["approvalLogId"] = this.approvalLogId;
      data["uploadedById"] = this.uploadedById;
      data["fileName"] = this.fileName;
      data["fileTitle"] = this.fileTitle;
      data["filePath"] = this.filePath;
      data["dateCreated"] = this.dateCreated ? this.dateCreated.toISOString() : <any>undefined;
      data["approvalLog"] = this.approvalLog ? this.approvalLog.toJSON() : <any>undefined;
      data["id"] = this.id;
      return data; 
  }

  clone(): ApprovalLogDoc {
      const json = this.toJSON();
      let result = new ApprovalLogDoc();
      result.init(json);
      return result;
  }
}

export interface IApprovalLogDoc {
  approvalLogId: number;
  uploadedById: number;
  fileName: string | undefined;
  fileTitle: string | undefined;
  filePath: string | undefined;
  dateCreated: moment.Moment;
  approvalLog: ApprovalLog;
  id: number;
}

export class MessageOut implements IMessageOut {
  message: string | undefined;
  isSuccessful: boolean;

  constructor(data?: IMessageOut) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.message = _data["message"];
          this.isSuccessful = _data["isSuccessful"];
      }
  }

  static fromJS(data: any): MessageOut {
      data = typeof data === 'object' ? data : {};
      let result = new MessageOut();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["message"] = this.message;
      data["isSuccessful"] = this.isSuccessful;
      return data; 
  }

  clone(): MessageOut {
      const json = this.toJSON();
      let result = new MessageOut();
      result.init(json);
      return result;
  }
}

export interface IMessageOut {
  message: string | undefined;
  isSuccessful: boolean;
}

export class MessageOutApiResult implements IMessageOutApiResult {
  hasError: boolean;
  message: string | undefined;
  result: MessageOut;

  constructor(data?: IMessageOutApiResult) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.hasError = _data["hasError"];
          this.message = _data["message"];
          this.result = _data["result"] ? MessageOut.fromJS(_data["result"]) : <any>undefined;
      }
  }

  static fromJS(data: any): MessageOutApiResult {
      data = typeof data === 'object' ? data : {};
      let result = new MessageOutApiResult();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["hasError"] = this.hasError;
      data["message"] = this.message;
      data["result"] = this.result ? this.result.toJSON() : <any>undefined;
      return data; 
  }

  clone(): MessageOutApiResult {
      const json = this.toJSON();
      let result = new MessageOutApiResult();
      result.init(json);
      return result;
  }
}

export interface IMessageOutApiResult {
  hasError: boolean;
  message: string | undefined;
  result: MessageOut;
}

export class AppLogMessageDTO implements IAppLogMessageDTO {
  id: number;
  formSectionId: number;
  formSectionName: string | undefined;
  postedBy: string | undefined;
  datePosted: moment.Moment;
  message: string | undefined;
  applicationId: number;
  applicationReviewId: number;

  constructor(data?: IAppLogMessageDTO) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.id = _data["id"];
          this.formSectionId = _data["formSectionId"];
          this.formSectionName = _data["formSectionName"];
          this.postedBy = _data["postedBy"];
          this.datePosted = _data["datePosted"] ? moment(_data["datePosted"].toString()) : <any>undefined;
          this.message = _data["message"];
          this.applicationId = _data["applicationId"];
          this.applicationReviewId = _data["applicationReviewId"];
      }
  }

  static fromJS(data: any): AppLogMessageDTO {
      data = typeof data === 'object' ? data : {};
      let result = new AppLogMessageDTO();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["id"] = this.id;
      data["formSectionId"] = this.formSectionId;
      data["formSectionName"] = this.formSectionName;
      data["postedBy"] = this.postedBy;
      data["datePosted"] = this.datePosted ? this.datePosted.toISOString() : <any>undefined;
      data["message"] = this.message;
      data["applicationId"] = this.applicationId;
      data["applicationReviewId"] = this.applicationReviewId;
      return data; 
  }

  clone(): AppLogMessageDTO {
      const json = this.toJSON();
      let result = new AppLogMessageDTO();
      result.init(json);
      return result;
  }
}

export interface IAppLogMessageDTO {
  id: number;
  formSectionId: number;
  formSectionName: string | undefined;
  postedBy: string | undefined;
  datePosted: moment.Moment;
  message: string | undefined;
  applicationId: number;
  applicationReviewId: number;
}

export class AppLogMessageDTOIListApiResult implements IAppLogMessageDTOIListApiResult {
  hasError: boolean;
  message: string | undefined;
  result: AppLogMessageDTO[] | undefined;

  constructor(data?: IAppLogMessageDTOIListApiResult) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.hasError = _data["hasError"];
          this.message = _data["message"];
          if (Array.isArray(_data["result"])) {
              this.result = [] as any;
              for (let item of _data["result"])
                  this.result.push(AppLogMessageDTO.fromJS(item));
          }
      }
  }

  static fromJS(data: any): AppLogMessageDTOIListApiResult {
      data = typeof data === 'object' ? data : {};
      let result = new AppLogMessageDTOIListApiResult();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["hasError"] = this.hasError;
      data["message"] = this.message;
      if (Array.isArray(this.result)) {
          data["result"] = [];
          for (let item of this.result)
              data["result"].push(item.toJSON());
      }
      return data; 
  }

  clone(): AppLogMessageDTOIListApiResult {
      const json = this.toJSON();
      let result = new AppLogMessageDTOIListApiResult();
      result.init(json);
      return result;
  }
}

export interface IAppLogMessageDTOIListApiResult {
  hasError: boolean;
  message: string | undefined;
  result: AppLogMessageDTO[] | undefined;
}

export class BooleanApiResult implements IBooleanApiResult {
  hasError: boolean;
  message: string | undefined;
  result: boolean;

  constructor(data?: IBooleanApiResult) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.hasError = _data["hasError"];
          this.message = _data["message"];
          this.result = _data["result"];
      }
  }

  static fromJS(data: any): BooleanApiResult {
      data = typeof data === 'object' ? data : {};
      let result = new BooleanApiResult();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["hasError"] = this.hasError;
      data["message"] = this.message;
      data["result"] = this.result;
      return data; 
  }

  clone(): BooleanApiResult {
      const json = this.toJSON();
      let result = new BooleanApiResult();
      result.init(json);
      return result;
  }
}

export interface IBooleanApiResult {
  hasError: boolean;
  message: string | undefined;
  result: boolean;
}

export class ApprovalResponseDTO implements IApprovalResponseDTO {
  status: boolean;
  message: string | undefined;
  next_approver: string | undefined;

  constructor(data?: IApprovalResponseDTO) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.status = _data["status"];
          this.message = _data["message"];
          this.next_approver = _data["next_approver"];
      }
  }

  static fromJS(data: any): ApprovalResponseDTO {
      data = typeof data === 'object' ? data : {};
      let result = new ApprovalResponseDTO();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["status"] = this.status;
      data["message"] = this.message;
      data["next_approver"] = this.next_approver;
      return data; 
  }

  clone(): ApprovalResponseDTO {
      const json = this.toJSON();
      let result = new ApprovalResponseDTO();
      result.init(json);
      return result;
  }
}

export interface IApprovalResponseDTO {
  status: boolean;
  message: string | undefined;
  next_approver: string | undefined;
}

export class ApprovalResponseDTOApiResult implements IApprovalResponseDTOApiResult {
  hasError: boolean;
  message: string | undefined;
  result: ApprovalResponseDTO;

  constructor(data?: IApprovalResponseDTOApiResult) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.hasError = _data["hasError"];
          this.message = _data["message"];
          this.result = _data["result"] ? ApprovalResponseDTO.fromJS(_data["result"]) : <any>undefined;
      }
  }

  static fromJS(data: any): ApprovalResponseDTOApiResult {
      data = typeof data === 'object' ? data : {};
      let result = new ApprovalResponseDTOApiResult();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["hasError"] = this.hasError;
      data["message"] = this.message;
      data["result"] = this.result ? this.result.toJSON() : <any>undefined;
      return data; 
  }

  clone(): ApprovalResponseDTOApiResult {
      const json = this.toJSON();
      let result = new ApprovalResponseDTOApiResult();
      result.init(json);
      return result;
  }
}

export interface IApprovalResponseDTOApiResult {
  hasError: boolean;
  message: string | undefined;
  result: ApprovalResponseDTO;
}

export class ApprovalLogDTO implements IApprovalLogDTO {
  id: number;
  approvalProcessId: number;
  processName: string | undefined;
  roleName: string | undefined;
  roleId: number;
  stepSn: number;
  itemId: number;
  title: string | undefined;
  notifyDate: moment.Moment | undefined;
  pendingDuration: string | undefined;
  description: string | undefined;
  stepLabel: string | undefined;
  fullName: string | undefined;

  constructor(data?: IApprovalLogDTO) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.id = _data["id"];
          this.approvalProcessId = _data["approvalProcessId"];
          this.processName = _data["processName"];
          this.roleName = _data["roleName"];
          this.roleId = _data["roleId"];
          this.stepSn = _data["stepSn"];
          this.itemId = _data["itemId"];
          this.title = _data["title"];
          this.notifyDate = _data["notifyDate"] ? moment(_data["notifyDate"].toString()) : <any>undefined;
          this.pendingDuration = _data["pendingDuration"];
          this.description = _data["description"];
          this.stepLabel = _data["stepLabel"];
          this.fullName = _data["fullName"];
      }
  }

  static fromJS(data: any): ApprovalLogDTO {
      data = typeof data === 'object' ? data : {};
      let result = new ApprovalLogDTO();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["id"] = this.id;
      data["approvalProcessId"] = this.approvalProcessId;
      data["processName"] = this.processName;
      data["roleName"] = this.roleName;
      data["roleId"] = this.roleId;
      data["stepSn"] = this.stepSn;
      data["itemId"] = this.itemId;
      data["title"] = this.title;
      data["notifyDate"] = this.notifyDate ? this.notifyDate.toISOString() : <any>undefined;
      data["pendingDuration"] = this.pendingDuration;
      data["description"] = this.description;
      data["stepLabel"] = this.stepLabel;
      data["fullName"] = this.fullName;
      return data; 
  }

  clone(): ApprovalLogDTO {
      const json = this.toJSON();
      let result = new ApprovalLogDTO();
      result.init(json);
      return result;
  }
}

export interface IApprovalLogDTO {
  id: number;
  approvalProcessId: number;
  processName: string | undefined;
  roleName: string | undefined;
  roleId: number;
  stepSn: number;
  itemId: number;
  title: string | undefined;
  notifyDate: moment.Moment | undefined;
  pendingDuration: string | undefined;
  description: string | undefined;
  stepLabel: string | undefined;
  fullName: string | undefined;
}

export class ApprovalLogDTOIListApiResult implements IApprovalLogDTOIListApiResult {
  hasError: boolean;
  message: string | undefined;
  result: ApprovalLogDTO[] | undefined;

  constructor(data?: IApprovalLogDTOIListApiResult) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.hasError = _data["hasError"];
          this.message = _data["message"];
          if (Array.isArray(_data["result"])) {
              this.result = [] as any;
              for (let item of _data["result"])
                  this.result.push(ApprovalLogDTO.fromJS(item));
          }
      }
  }

  static fromJS(data: any): ApprovalLogDTOIListApiResult {
      data = typeof data === 'object' ? data : {};
      let result = new ApprovalLogDTOIListApiResult();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["hasError"] = this.hasError;
      data["message"] = this.message;
      if (Array.isArray(this.result)) {
          data["result"] = [];
          for (let item of this.result)
              data["result"].push(item.toJSON());
      }
      return data; 
  }

  clone(): ApprovalLogDTOIListApiResult {
      const json = this.toJSON();
      let result = new ApprovalLogDTOIListApiResult();
      result.init(json);
      return result;
  }
}

export interface IApprovalLogDTOIListApiResult {
  hasError: boolean;
  message: string | undefined;
  result: ApprovalLogDTO[] | undefined;
}

export class Int32ApiResult implements IInt32ApiResult {
  hasError: boolean;
  message: string | undefined;
  result: number;

  constructor(data?: IInt32ApiResult) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.hasError = _data["hasError"];
          this.message = _data["message"];
          this.result = _data["result"];
      }
  }

  static fromJS(data: any): Int32ApiResult {
      data = typeof data === 'object' ? data : {};
      let result = new Int32ApiResult();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["hasError"] = this.hasError;
      data["message"] = this.message;
      data["result"] = this.result;
      return data; 
  }

  clone(): Int32ApiResult {
      const json = this.toJSON();
      let result = new Int32ApiResult();
      result.init(json);
      return result;
  }
}

export interface IInt32ApiResult {
  hasError: boolean;
  message: string | undefined;
  result: number;
}

export class FormSection implements IFormSection {
  name: string | undefined;
  id: number;

  constructor(data?: IFormSection) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.name = _data["name"];
          this.id = _data["id"];
      }
  }

  static fromJS(data: any): FormSection {
      data = typeof data === 'object' ? data : {};
      let result = new FormSection();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["name"] = this.name;
      data["id"] = this.id;
      return data; 
  }

  clone(): FormSection {
      const json = this.toJSON();
      let result = new FormSection();
      result.init(json);
      return result;
  }
}

export interface IFormSection {
  name: string | undefined;
  id: number;
}

export class FormSectionIListApiResult implements IFormSectionIListApiResult {
  hasError: boolean;
  message: string | undefined;
  result: FormSection[] | undefined;

  constructor(data?: IFormSectionIListApiResult) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.hasError = _data["hasError"];
          this.message = _data["message"];
          if (Array.isArray(_data["result"])) {
              this.result = [] as any;
              for (let item of _data["result"])
                  this.result.push(FormSection.fromJS(item));
          }
      }
  }

  static fromJS(data: any): FormSectionIListApiResult {
      data = typeof data === 'object' ? data : {};
      let result = new FormSectionIListApiResult();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["hasError"] = this.hasError;
      data["message"] = this.message;
      if (Array.isArray(this.result)) {
          data["result"] = [];
          for (let item of this.result)
              data["result"].push(item.toJSON());
      }
      return data; 
  }

  clone(): FormSectionIListApiResult {
      const json = this.toJSON();
      let result = new FormSectionIListApiResult();
      result.init(json);
      return result;
  }
}

export interface IFormSectionIListApiResult {
  hasError: boolean;
  message: string | undefined;
  result: FormSection[] | undefined;
}

export class ApprovalProcess implements IApprovalProcess {
  name: string | undefined;
  description: string | undefined;
  id: number;

  constructor(data?: IApprovalProcess) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.name = _data["name"];
          this.description = _data["description"];
          this.id = _data["id"];
      }
  }

  static fromJS(data: any): ApprovalProcess {
      data = typeof data === 'object' ? data : {};
      let result = new ApprovalProcess();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["name"] = this.name;
      data["description"] = this.description;
      data["id"] = this.id;
      return data; 
  }

  clone(): ApprovalProcess {
      const json = this.toJSON();
      let result = new ApprovalProcess();
      result.init(json);
      return result;
  }
}

export interface IApprovalProcess {
  name: string | undefined;
  description: string | undefined;
  id: number;
}

export class ApprovalProcessIListApiResult implements IApprovalProcessIListApiResult {
  hasError: boolean;
  message: string | undefined;
  result: ApprovalProcess[] | undefined;

  constructor(data?: IApprovalProcessIListApiResult) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.hasError = _data["hasError"];
          this.message = _data["message"];
          if (Array.isArray(_data["result"])) {
              this.result = [] as any;
              for (let item of _data["result"])
                  this.result.push(ApprovalProcess.fromJS(item));
          }
      }
  }

  static fromJS(data: any): ApprovalProcessIListApiResult {
      data = typeof data === 'object' ? data : {};
      let result = new ApprovalProcessIListApiResult();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["hasError"] = this.hasError;
      data["message"] = this.message;
      if (Array.isArray(this.result)) {
          data["result"] = [];
          for (let item of this.result)
              data["result"].push(item.toJSON());
      }
      return data; 
  }

  clone(): ApprovalProcessIListApiResult {
      const json = this.toJSON();
      let result = new ApprovalProcessIListApiResult();
      result.init(json);
      return result;
  }
}

export interface IApprovalProcessIListApiResult {
  hasError: boolean;
  message: string | undefined;
  result: ApprovalProcess[] | undefined;
}

export class ApprovalStep implements IApprovalStep {
  approvalProcessId: number;
  stepSn: number;
  roleId: number;
  userId: number;
  stepType: string | undefined;
  stepName: string | undefined;
  description: string | undefined;
  actionTags: string | undefined;
  swimLane: number;
  hasEscalation: boolean;
  escalationHours: number;
  escalationPrivilegeId: number;
  escalationsUserId: number;
  approvalDefaultMsg: string | undefined;
  rejectionDefaultMsg: string | undefined;
  approvalButtonText: string | undefined;
  rejectionButtonText: string | undefined;
  id: number;

  constructor(data?: IApprovalStep) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.approvalProcessId = _data["approvalProcessId"];
          this.stepSn = _data["stepSn"];
          this.roleId = _data["roleId"];
          this.userId = _data["userId"];
          this.stepType = _data["stepType"];
          this.stepName = _data["stepName"];
          this.description = _data["description"];
          this.actionTags = _data["actionTags"];
          this.swimLane = _data["swimLane"];
          this.hasEscalation = _data["hasEscalation"];
          this.escalationHours = _data["escalationHours"];
          this.escalationPrivilegeId = _data["escalationPrivilegeId"];
          this.escalationsUserId = _data["escalationsUserId"];
          this.approvalDefaultMsg = _data["approvalDefaultMsg"];
          this.rejectionDefaultMsg = _data["rejectionDefaultMsg"];
          this.approvalButtonText = _data["approvalButtonText"];
          this.rejectionButtonText = _data["rejectionButtonText"];
          this.id = _data["id"];
      }
  }

  static fromJS(data: any): ApprovalStep {
      data = typeof data === 'object' ? data : {};
      let result = new ApprovalStep();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["approvalProcessId"] = this.approvalProcessId;
      data["stepSn"] = this.stepSn;
      data["roleId"] = this.roleId;
      data["userId"] = this.userId;
      data["stepType"] = this.stepType;
      data["stepName"] = this.stepName;
      data["description"] = this.description;
      data["actionTags"] = this.actionTags;
      data["swimLane"] = this.swimLane;
      data["hasEscalation"] = this.hasEscalation;
      data["escalationHours"] = this.escalationHours;
      data["escalationPrivilegeId"] = this.escalationPrivilegeId;
      data["escalationsUserId"] = this.escalationsUserId;
      data["approvalDefaultMsg"] = this.approvalDefaultMsg;
      data["rejectionDefaultMsg"] = this.rejectionDefaultMsg;
      data["approvalButtonText"] = this.approvalButtonText;
      data["rejectionButtonText"] = this.rejectionButtonText;
      data["id"] = this.id;
      return data; 
  }

  clone(): ApprovalStep {
      const json = this.toJSON();
      let result = new ApprovalStep();
      result.init(json);
      return result;
  }
}

export interface IApprovalStep {
  approvalProcessId: number;
  stepSn: number;
  roleId: number;
  userId: number;
  stepType: string | undefined;
  stepName: string | undefined;
  description: string | undefined;
  actionTags: string | undefined;
  swimLane: number;
  hasEscalation: boolean;
  escalationHours: number;
  escalationPrivilegeId: number;
  escalationsUserId: number;
  approvalDefaultMsg: string | undefined;
  rejectionDefaultMsg: string | undefined;
  approvalButtonText: string | undefined;
  rejectionButtonText: string | undefined;
  id: number;
}

export class ApprovalStepIListApiResult implements IApprovalStepIListApiResult {
  hasError: boolean;
  message: string | undefined;
  result: ApprovalStep[] | undefined;

  constructor(data?: IApprovalStepIListApiResult) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.hasError = _data["hasError"];
          this.message = _data["message"];
          if (Array.isArray(_data["result"])) {
              this.result = [] as any;
              for (let item of _data["result"])
                  this.result.push(ApprovalStep.fromJS(item));
          }
      }
  }

  static fromJS(data: any): ApprovalStepIListApiResult {
      data = typeof data === 'object' ? data : {};
      let result = new ApprovalStepIListApiResult();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["hasError"] = this.hasError;
      data["message"] = this.message;
      if (Array.isArray(this.result)) {
          data["result"] = [];
          for (let item of this.result)
              data["result"].push(item.toJSON());
      }
      return data; 
  }

  clone(): ApprovalStepIListApiResult {
      const json = this.toJSON();
      let result = new ApprovalStepIListApiResult();
      result.init(json);
      return result;
  }
}

export interface IApprovalStepIListApiResult {
  hasError: boolean;
  message: string | undefined;
  result: ApprovalStep[] | undefined;
}

export class ChangeUiThemeInput implements IChangeUiThemeInput {
  theme: string;

  constructor(data?: IChangeUiThemeInput) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.theme = _data["theme"];
      }
  }

  static fromJS(data: any): ChangeUiThemeInput {
      data = typeof data === 'object' ? data : {};
      let result = new ChangeUiThemeInput();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["theme"] = this.theme;
      return data; 
  }

  clone(): ChangeUiThemeInput {
      const json = this.toJSON();
      let result = new ChangeUiThemeInput();
      result.init(json);
      return result;
  }
}

export interface IChangeUiThemeInput {
  theme: string;
}

export class DocTemplate implements IDocTemplate {
  approvalProcessId: number;
  name: string | undefined;
  template: string | undefined;
  id: number;

  constructor(data?: IDocTemplate) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.approvalProcessId = _data["approvalProcessId"];
          this.name = _data["name"];
          this.template = _data["template"];
          this.id = _data["id"];
      }
  }

  static fromJS(data: any): DocTemplate {
      data = typeof data === 'object' ? data : {};
      let result = new DocTemplate();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["approvalProcessId"] = this.approvalProcessId;
      data["name"] = this.name;
      data["template"] = this.template;
      data["id"] = this.id;
      return data; 
  }

  clone(): DocTemplate {
      const json = this.toJSON();
      let result = new DocTemplate();
      result.init(json);
      return result;
  }
}

export interface IDocTemplate {
  approvalProcessId: number;
  name: string | undefined;
  template: string | undefined;
  id: number;
}

export class DocTemplateIListApiResult implements IDocTemplateIListApiResult {
  hasError: boolean;
  message: string | undefined;
  result: DocTemplate[] | undefined;

  constructor(data?: IDocTemplateIListApiResult) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.hasError = _data["hasError"];
          this.message = _data["message"];
          if (Array.isArray(_data["result"])) {
              this.result = [] as any;
              for (let item of _data["result"])
                  this.result.push(DocTemplate.fromJS(item));
          }
      }
  }

  static fromJS(data: any): DocTemplateIListApiResult {
      data = typeof data === 'object' ? data : {};
      let result = new DocTemplateIListApiResult();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["hasError"] = this.hasError;
      data["message"] = this.message;
      if (Array.isArray(this.result)) {
          data["result"] = [];
          for (let item of this.result)
              data["result"].push(item.toJSON());
      }
      return data; 
  }

  clone(): DocTemplateIListApiResult {
      const json = this.toJSON();
      let result = new DocTemplateIListApiResult();
      result.init(json);
      return result;
  }
}

export interface IDocTemplateIListApiResult {
  hasError: boolean;
  message: string | undefined;
  result: DocTemplate[] | undefined;
}

export class DocTemplateApiResult implements IDocTemplateApiResult {
  hasError: boolean;
  message: string | undefined;
  result: DocTemplate;

  constructor(data?: IDocTemplateApiResult) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.hasError = _data["hasError"];
          this.message = _data["message"];
          this.result = _data["result"] ? DocTemplate.fromJS(_data["result"]) : <any>undefined;
      }
  }

  static fromJS(data: any): DocTemplateApiResult {
      data = typeof data === 'object' ? data : {};
      let result = new DocTemplateApiResult();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["hasError"] = this.hasError;
      data["message"] = this.message;
      data["result"] = this.result ? this.result.toJSON() : <any>undefined;
      return data; 
  }

  clone(): DocTemplateApiResult {
      const json = this.toJSON();
      let result = new DocTemplateApiResult();
      result.init(json);
      return result;
  }
}

export interface IDocTemplateApiResult {
  hasError: boolean;
  message: string | undefined;
  result: DocTemplate;
}



@Injectable()
export class RoleServiceProxy {
    private http: HttpClient;
    private baseUrl: string;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

    constructor(private _tokenService: TokenService, @Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
        this.http = http;
        this.baseUrl = baseUrl ? baseUrl : "";
    }

    /**
     * @param body (optional) 
     * @return Success
     */
    create(body: CreateRoleDto | undefined): Observable<RoleDto> {
        let url_ = this.baseUrl + "/api/services/app/Role/Create";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_ : any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain",
                'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processCreate(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processCreate(<any>response_);
                } catch (e) {
                    return <Observable<RoleDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<RoleDto>><any>_observableThrow(response_);
        }));
    }

    protected processCreate(response: HttpResponseBase): Observable<RoleDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = RoleDto.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<RoleDto>(<any>null);
    }

    /**
     * @param permission (optional) 
     * @return Success
     */
    getRoles(permission: string | null | undefined): Observable<RoleListDtoListResultDto> {
        let url_ = this.baseUrl + "/api/services/app/Role/GetRoles?";
        if (permission !== undefined && permission !== null)
            url_ += "Permission=" + encodeURIComponent("" + permission) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
                'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processGetRoles(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetRoles(<any>response_);
                } catch (e) {
                    return <Observable<RoleListDtoListResultDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<RoleListDtoListResultDto>><any>_observableThrow(response_);
        }));
    }

    protected processGetRoles(response: HttpResponseBase): Observable<RoleListDtoListResultDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = RoleListDtoListResultDto.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<RoleListDtoListResultDto>(<any>null);
    }

    /**
     * @param body (optional) 
     * @return Success
     */
    update(body: RoleDto | undefined): Observable<RoleDto> {
        let url_ = this.baseUrl + "/api/services/app/Role/Update";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_ : any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain",
                'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("put", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processUpdate(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processUpdate(<any>response_);
                } catch (e) {
                    return <Observable<RoleDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<RoleDto>><any>_observableThrow(response_);
        }));
    }

    protected processUpdate(response: HttpResponseBase): Observable<RoleDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = RoleDto.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<RoleDto>(<any>null);
    }

    /**
     * @param id (optional) 
     * @return Success
     */
    delete(id: number | undefined): Observable<void> {
        let url_ = this.baseUrl + "/api/services/app/Role/Delete?";
        if (id === null)
            throw new Error("The parameter 'id' cannot be null.");
        else if (id !== undefined)
            url_ += "Id=" + encodeURIComponent("" + id) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("delete", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processDelete(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processDelete(<any>response_);
                } catch (e) {
                    return <Observable<void>><any>_observableThrow(e);
                }
            } else
                return <Observable<void>><any>_observableThrow(response_);
        }));
    }

    protected processDelete(response: HttpResponseBase): Observable<void> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return _observableOf<void>(<any>null);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<void>(<any>null);
    }

    /**
     * @return Success
     */
    getAllPermissions(): Observable<PermissionDtoListResultDto> {
        let url_ = this.baseUrl + "/api/services/app/Role/GetAllPermissions";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
                'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processGetAllPermissions(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetAllPermissions(<any>response_);
                } catch (e) {
                    return <Observable<PermissionDtoListResultDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<PermissionDtoListResultDto>><any>_observableThrow(response_);
        }));
    }

    protected processGetAllPermissions(response: HttpResponseBase): Observable<PermissionDtoListResultDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = PermissionDtoListResultDto.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<PermissionDtoListResultDto>(<any>null);
    }

    /**
     * @param id (optional) 
     * @return Success
     */
    getRoleForEdit(id: number | undefined): Observable<GetRoleForEditOutput> {
        let url_ = this.baseUrl + "/api/services/app/Role/GetRoleForEdit?";
        if (id === null)
            throw new Error("The parameter 'id' cannot be null.");
        else if (id !== undefined)
            url_ += "Id=" + encodeURIComponent("" + id) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
                'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processGetRoleForEdit(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetRoleForEdit(<any>response_);
                } catch (e) {
                    return <Observable<GetRoleForEditOutput>><any>_observableThrow(e);
                }
            } else
                return <Observable<GetRoleForEditOutput>><any>_observableThrow(response_);
        }));
    }

    protected processGetRoleForEdit(response: HttpResponseBase): Observable<GetRoleForEditOutput> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = GetRoleForEditOutput.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<GetRoleForEditOutput>(<any>null);
    }

    /**
     * @param id (optional) 
     * @return Success
     */
    get(id: number | undefined): Observable<RoleDto> {
        let url_ = this.baseUrl + "/api/services/app/Role/Get?";
        if (id === null)
            throw new Error("The parameter 'id' cannot be null.");
        else if (id !== undefined)
            url_ += "Id=" + encodeURIComponent("" + id) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
                'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processGet(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGet(<any>response_);
                } catch (e) {
                    return <Observable<RoleDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<RoleDto>><any>_observableThrow(response_);
        }));
    }

    protected processGet(response: HttpResponseBase): Observable<RoleDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = RoleDto.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<RoleDto>(<any>null);
    }

    /**
     * @param keyword (optional) 
     * @param skipCount (optional) 
     * @param maxResultCount (optional) 
     * @return Success
     */
    getAll(keyword: string | null | undefined, skipCount: number | undefined, maxResultCount: number | undefined): Observable<RoleDtoPagedResultDto> {
        let url_ = this.baseUrl + "/api/services/app/Role/GetAll?";
        if (keyword !== undefined && keyword !== null)
            url_ += "Keyword=" + encodeURIComponent("" + keyword) + "&";
        if (skipCount === null)
            throw new Error("The parameter 'skipCount' cannot be null.");
        else if (skipCount !== undefined)
            url_ += "SkipCount=" + encodeURIComponent("" + skipCount) + "&";
        if (maxResultCount === null)
            throw new Error("The parameter 'maxResultCount' cannot be null.");
        else if (maxResultCount !== undefined)
            url_ += "MaxResultCount=" + encodeURIComponent("" + maxResultCount) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ : any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain",
                'Authorization': 'Bearer ' + this._tokenService.getToken()
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_ : any) => {
            return this.processGetAll(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetAll(<any>response_);
                } catch (e) {
                    return <Observable<RoleDtoPagedResultDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<RoleDtoPagedResultDto>><any>_observableThrow(response_);
        }));
    }

    protected processGetAll(response: HttpResponseBase): Observable<RoleDtoPagedResultDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }}
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = RoleDtoPagedResultDto.fromJS(resultData200);
            return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<RoleDtoPagedResultDto>(<any>null);
    }
}


export class CreateRoleDto implements ICreateRoleDto {
    name: string;
    displayName: string;
    normalizedName: string | undefined;
    description: string | undefined;
    grantedPermissions: string[] | undefined;

    constructor(data?: ICreateRoleDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(_data?: any) {
        if (_data) {
            this.name = _data["name"];
            this.displayName = _data["displayName"];
            this.normalizedName = _data["normalizedName"];
            this.description = _data["description"];
            if (Array.isArray(_data["grantedPermissions"])) {
                this.grantedPermissions = [] as any;
                for (let item of _data["grantedPermissions"])
                    this.grantedPermissions.push(item);
            }
        }
    }

    static fromJS(data: any): CreateRoleDto {
        data = typeof data === 'object' ? data : {};
        let result = new CreateRoleDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["displayName"] = this.displayName;
        data["normalizedName"] = this.normalizedName;
        data["description"] = this.description;
        if (Array.isArray(this.grantedPermissions)) {
            data["grantedPermissions"] = [];
            for (let item of this.grantedPermissions)
                data["grantedPermissions"].push(item);
        }
        return data; 
    }

    clone(): CreateRoleDto {
        const json = this.toJSON();
        let result = new CreateRoleDto();
        result.init(json);
        return result;
    }
}

export interface ICreateRoleDto {
    name: string;
    displayName: string;
    normalizedName: string | undefined;
    description: string | undefined;
    grantedPermissions: string[] | undefined;
}

export class RoleDto implements IRoleDto {
    name: string;
    displayName: string;
    normalizedName: string | undefined;
    description: string | undefined;
    grantedPermissions: string[] | undefined;
    id: number;

    constructor(data?: IRoleDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(_data?: any) {
        if (_data) {
            this.name = _data["name"];
            this.displayName = _data["displayName"];
            this.normalizedName = _data["normalizedName"];
            this.description = _data["description"];
            if (Array.isArray(_data["grantedPermissions"])) {
                this.grantedPermissions = [] as any;
                for (let item of _data["grantedPermissions"])
                    this.grantedPermissions.push(item);
            }
            this.id = _data["id"];
        }
    }

    static fromJS(data: any): RoleDto {
        data = typeof data === 'object' ? data : {};
        let result = new RoleDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["displayName"] = this.displayName;
        data["normalizedName"] = this.normalizedName;
        data["description"] = this.description;
        if (Array.isArray(this.grantedPermissions)) {
            data["grantedPermissions"] = [];
            for (let item of this.grantedPermissions)
                data["grantedPermissions"].push(item);
        }
        data["id"] = this.id;
        return data; 
    }

    clone(): RoleDto {
        const json = this.toJSON();
        let result = new RoleDto();
        result.init(json);
        return result;
    }
}

export interface IRoleDto {
    name: string;
    displayName: string;
    normalizedName: string | undefined;
    description: string | undefined;
    grantedPermissions: string[] | undefined;
    id: number;
}

export class RoleListDto implements IRoleListDto {
    name: string | undefined;
    displayName: string | undefined;
    isStatic: boolean;
    isDefault: boolean;
    creationTime: moment.Moment;
    id: number;

    constructor(data?: IRoleListDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(_data?: any) {
        if (_data) {
            this.name = _data["name"];
            this.displayName = _data["displayName"];
            this.isStatic = _data["isStatic"];
            this.isDefault = _data["isDefault"];
            this.creationTime = _data["creationTime"] ? moment(_data["creationTime"].toString()) : <any>undefined;
            this.id = _data["id"];
        }
    }

    static fromJS(data: any): RoleListDto {
        data = typeof data === 'object' ? data : {};
        let result = new RoleListDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["displayName"] = this.displayName;
        data["isStatic"] = this.isStatic;
        data["isDefault"] = this.isDefault;
        data["creationTime"] = this.creationTime ? this.creationTime.toISOString() : <any>undefined;
        data["id"] = this.id;
        return data; 
    }

    clone(): RoleListDto {
        const json = this.toJSON();
        let result = new RoleListDto();
        result.init(json);
        return result;
    }
}

export interface IRoleListDto {
    name: string | undefined;
    displayName: string | undefined;
    isStatic: boolean;
    isDefault: boolean;
    creationTime: moment.Moment;
    id: number;
}

export class RoleListDtoListResultDto implements IRoleListDtoListResultDto {
    items: RoleListDto[] | undefined;

    constructor(data?: IRoleListDtoListResultDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(_data?: any) {
        if (_data) {
            if (Array.isArray(_data["items"])) {
                this.items = [] as any;
                for (let item of _data["items"])
                    this.items.push(RoleListDto.fromJS(item));
            }
        }
    }

    static fromJS(data: any): RoleListDtoListResultDto {
        data = typeof data === 'object' ? data : {};
        let result = new RoleListDtoListResultDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        if (Array.isArray(this.items)) {
            data["items"] = [];
            for (let item of this.items)
                data["items"].push(item.toJSON());
        }
        return data; 
    }

    clone(): RoleListDtoListResultDto {
        const json = this.toJSON();
        let result = new RoleListDtoListResultDto();
        result.init(json);
        return result;
    }
}

export interface IRoleListDtoListResultDto {
    items: RoleListDto[] | undefined;
}

export class PermissionDto implements IPermissionDto {
    name: string | undefined;
    displayName: string | undefined;
    description: string | undefined;
    id: number;

    constructor(data?: IPermissionDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(_data?: any) {
        if (_data) {
            this.name = _data["name"];
            this.displayName = _data["displayName"];
            this.description = _data["description"];
            this.id = _data["id"];
        }
    }

    static fromJS(data: any): PermissionDto {
        data = typeof data === 'object' ? data : {};
        let result = new PermissionDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["displayName"] = this.displayName;
        data["description"] = this.description;
        data["id"] = this.id;
        return data; 
    }

    clone(): PermissionDto {
        const json = this.toJSON();
        let result = new PermissionDto();
        result.init(json);
        return result;
    }
}

export interface IPermissionDto {
    name: string | undefined;
    displayName: string | undefined;
    description: string | undefined;
    id: number;
}

export class PermissionDtoListResultDto implements IPermissionDtoListResultDto {
    items: PermissionDto[] | undefined;

    constructor(data?: IPermissionDtoListResultDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(_data?: any) {
        if (_data) {
            if (Array.isArray(_data["items"])) {
                this.items = [] as any;
                for (let item of _data["items"])
                    this.items.push(PermissionDto.fromJS(item));
            }
        }
    }

    static fromJS(data: any): PermissionDtoListResultDto {
        data = typeof data === 'object' ? data : {};
        let result = new PermissionDtoListResultDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        if (Array.isArray(this.items)) {
            data["items"] = [];
            for (let item of this.items)
                data["items"].push(item.toJSON());
        }
        return data; 
    }

    clone(): PermissionDtoListResultDto {
        const json = this.toJSON();
        let result = new PermissionDtoListResultDto();
        result.init(json);
        return result;
    }
}

export interface IPermissionDtoListResultDto {
    items: PermissionDto[] | undefined;
}

export class RoleEditDto implements IRoleEditDto {
    name: string;
    displayName: string;
    description: string | undefined;
    isStatic: boolean;
    id: number;

    constructor(data?: IRoleEditDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(_data?: any) {
        if (_data) {
            this.name = _data["name"];
            this.displayName = _data["displayName"];
            this.description = _data["description"];
            this.isStatic = _data["isStatic"];
            this.id = _data["id"];
        }
    }

    static fromJS(data: any): RoleEditDto {
        data = typeof data === 'object' ? data : {};
        let result = new RoleEditDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["displayName"] = this.displayName;
        data["description"] = this.description;
        data["isStatic"] = this.isStatic;
        data["id"] = this.id;
        return data; 
    }

    clone(): RoleEditDto {
        const json = this.toJSON();
        let result = new RoleEditDto();
        result.init(json);
        return result;
    }
}

export interface IRoleEditDto {
    name: string;
    displayName: string;
    description: string | undefined;
    isStatic: boolean;
    id: number;
}

export class FlatPermissionDto implements IFlatPermissionDto {
    name: string | undefined;
    displayName: string | undefined;
    description: string | undefined;

    constructor(data?: IFlatPermissionDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(_data?: any) {
        if (_data) {
            this.name = _data["name"];
            this.displayName = _data["displayName"];
            this.description = _data["description"];
        }
    }

    static fromJS(data: any): FlatPermissionDto {
        data = typeof data === 'object' ? data : {};
        let result = new FlatPermissionDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["displayName"] = this.displayName;
        data["description"] = this.description;
        return data; 
    }

    clone(): FlatPermissionDto {
        const json = this.toJSON();
        let result = new FlatPermissionDto();
        result.init(json);
        return result;
    }
}

export interface IFlatPermissionDto {
    name: string | undefined;
    displayName: string | undefined;
    description: string | undefined;
}

export class GetRoleForEditOutput implements IGetRoleForEditOutput {
    role: RoleEditDto;
    permissions: FlatPermissionDto[] | undefined;
    grantedPermissionNames: string[] | undefined;

    constructor(data?: IGetRoleForEditOutput) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(_data?: any) {
        if (_data) {
            this.role = _data["role"] ? RoleEditDto.fromJS(_data["role"]) : <any>undefined;
            if (Array.isArray(_data["permissions"])) {
                this.permissions = [] as any;
                for (let item of _data["permissions"])
                    this.permissions.push(FlatPermissionDto.fromJS(item));
            }
            if (Array.isArray(_data["grantedPermissionNames"])) {
                this.grantedPermissionNames = [] as any;
                for (let item of _data["grantedPermissionNames"])
                    this.grantedPermissionNames.push(item);
            }
        }
    }

    static fromJS(data: any): GetRoleForEditOutput {
        data = typeof data === 'object' ? data : {};
        let result = new GetRoleForEditOutput();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["role"] = this.role ? this.role.toJSON() : <any>undefined;
        if (Array.isArray(this.permissions)) {
            data["permissions"] = [];
            for (let item of this.permissions)
                data["permissions"].push(item.toJSON());
        }
        if (Array.isArray(this.grantedPermissionNames)) {
            data["grantedPermissionNames"] = [];
            for (let item of this.grantedPermissionNames)
                data["grantedPermissionNames"].push(item);
        }
        return data; 
    }

    clone(): GetRoleForEditOutput {
        const json = this.toJSON();
        let result = new GetRoleForEditOutput();
        result.init(json);
        return result;
    }
}

export interface IGetRoleForEditOutput {
    role: RoleEditDto;
    permissions: FlatPermissionDto[] | undefined;
    grantedPermissionNames: string[] | undefined;
}

export class RoleDtoPagedResultDto implements IRoleDtoPagedResultDto {
    totalCount: number;
    items: RoleDto[] | undefined;

    constructor(data?: IRoleDtoPagedResultDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(_data?: any) {
        if (_data) {
            this.totalCount = _data["totalCount"];
            if (Array.isArray(_data["items"])) {
                this.items = [] as any;
                for (let item of _data["items"])
                    this.items.push(RoleDto.fromJS(item));
            }
        }
    }

    static fromJS(data: any): RoleDtoPagedResultDto {
        data = typeof data === 'object' ? data : {};
        let result = new RoleDtoPagedResultDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["totalCount"] = this.totalCount;
        if (Array.isArray(this.items)) {
            data["items"] = [];
            for (let item of this.items)
                data["items"].push(item.toJSON());
        }
        return data; 
    }

    clone(): RoleDtoPagedResultDto {
        const json = this.toJSON();
        let result = new RoleDtoPagedResultDto();
        result.init(json);
        return result;
    }
}

export interface IRoleDtoPagedResultDto {
    totalCount: number;
    items: RoleDto[] | undefined;
}

export class ApplicationInfoDto implements IApplicationInfoDto {
    version: string | undefined;
    releaseDate: moment.Moment;
    features: { [key: string]: boolean; } | undefined;

    constructor(data?: IApplicationInfoDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(_data?: any) {
        if (_data) {
            this.version = _data["version"];
            this.releaseDate = _data["releaseDate"] ? moment(_data["releaseDate"].toString()) : <any>undefined;
            if (_data["features"]) {
                this.features = {} as any;
                for (let key in _data["features"]) {
                    if (_data["features"].hasOwnProperty(key))
                        this.features[key] = _data["features"][key];
                }
            }
        }
    }

    static fromJS(data: any): ApplicationInfoDto {
        data = typeof data === 'object' ? data : {};
        let result = new ApplicationInfoDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["version"] = this.version;
        data["releaseDate"] = this.releaseDate ? this.releaseDate.toISOString() : <any>undefined;
        if (this.features) {
            data["features"] = {};
            for (let key in this.features) {
                if (this.features.hasOwnProperty(key))
                    data["features"][key] = this.features[key];
            }
        }
        return data; 
    }

    clone(): ApplicationInfoDto {
        const json = this.toJSON();
        let result = new ApplicationInfoDto();
        result.init(json);
        return result;
    }
}

export interface IApplicationInfoDto {
    version: string | undefined;
    releaseDate: moment.Moment;
    features: { [key: string]: boolean; } | undefined;
}

export class ApprovalLogApiResult implements IApprovalLogApiResult {
    hasError: boolean;
    message: string | undefined;
    result: ApprovalLog;

    constructor(data?: IApprovalLogApiResult) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(_data?: any) {
        if (_data) {
            this.hasError = _data["hasError"];
            this.message = _data["message"];
            this.result = _data["result"] ? ApprovalLog.fromJS(_data["result"]) : <any>undefined;
        }
    }

    static fromJS(data: any): ApprovalLogApiResult {
        data = typeof data === 'object' ? data : {};
        let result = new ApprovalLogApiResult();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["hasError"] = this.hasError;
        data["message"] = this.message;
        data["result"] = this.result ? this.result.toJSON() : <any>undefined;
        return data; 
    }

    clone(): ApprovalLogApiResult {
        const json = this.toJSON();
        let result = new ApprovalLogApiResult();
        result.init(json);
        return result;
    }
}

export interface IApprovalLogApiResult {
    hasError: boolean;
    message: string | undefined;
    result: ApprovalLog;
}