import { Component, Injector, OnInit } from '@angular/core';
import { accountModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
  animations: [accountModuleAnimation()]
})
export class ForgotPasswordComponent implements OnInit {
  submitting = false;
  userNameOrEmailAddress = '';

  ngOnInit(): void {
  }

}
