import { Component, OnInit, Injector, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Utils } from '../../shared/helpers/Utils';

@Component({
  templateUrl: './home.component.html',
  animations: [appModuleAnimation()],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent extends AppComponentBase implements OnInit {
  @ViewChild('dt') table: Table;
  loading: Boolean = false;
  display: Boolean = false;
  displayAssign: Boolean = false;

  status = Utils.getStatus();

  statusOptions = Object.keys(this.status).map(item => {
    return { label: item, value: item.toLowerCase() };
  });


  summary = [
    {
      id: 1,
      sn: 1,
      issuer: 'GTB – Initial Longnametestbreaker ',
      name: 'Shawn Roberts0oooooo@gmail.comdafasdfasda on this is a long name i am testing',
      date: 'March 4, 2018',
      status: 'Unassigned'
    },
    {
      id: 2,
      sn: 2,
      issuer: 'Wema – Initial Listing ',
      name: 'thisIsAnExampleOfaVeryLongEmail@yahoo.com',
      date: 'March 4, 2018',
      status: 'Approved'
    }
  ];

  buttonItems = [
    {
      label: 'Resume', command: () => {
        console.log('assign clicked');
      }
    },
    {
      label: 'Assign', command: () => {
       this.showAssignDialog();
      }
    },
    {
      label: 'Action X', command: () => {
        console.log('action x clicked');
      }
    },
    {
      label: 'Action Y', command: () => {
        console.log('action y clicked');
      }
    },
    {
      label: 'Action Z', command: () => {
        console.log('action z clicked');
      }
    }
  ];

  cols = [
    { field: 'sn', header: 'S/N' },
    { field: 'issuer', header: 'Issuer' },
    { field: 'name', header: 'Application Name' },
    { field: 'date', header: 'Date' },
    { field: 'status', header: 'Status' },
    { field: 'action', header: 'Action' }
  ];

  months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
  }

  onDateSelect(value) {
    this.table.filter(this.formatDate(value), 'date', 'equals');
  }

  formatDate(date) {
    const month = date.getMonth();
    const day = date.getDate();

    return this.months[month] + ' ' + day + ' ' + date.getFullYear();
  }


  showDialog() {
    this.display = !this.display;
  }
  showAssignDialog() {
    this.displayAssign = !this.displayAssign;
  }

  toggleDialog() {
    this.display = !this.display;
  }

  toggleAssignDialog() {
    this.displayAssign = !this.displayAssign;
  }
}
