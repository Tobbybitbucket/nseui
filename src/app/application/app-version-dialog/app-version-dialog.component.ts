import { Component, OnInit, Input } from "@angular/core";
import { ApplicationServiceProxy } from "@shared/service-proxies/service-proxies";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { finalize } from "rxjs/operators";

@Component({
  selector: "app-version-dialog",
  templateUrl: "./app-version-dialog.component.html",
  styleUrls: ["./app-version-dialog.component.css"],
})
export class AppVersionDialogComponent implements OnInit {
  @Input() visible: boolean;
  @Input() data: any;
  constructor(
    private _applicationService: ApplicationServiceProxy,
    private ngxService: NgxUiLoaderService
  ) {}

  ngOnInit(): void {
    // console.log(this.data.appVersionRef);
    if (this.visible) {
      console.log(this.data);
      //   this.getApplicationVersions(this.data.appVersionRef);
    }
  }

  getApplicationVersions(ref): void {
    this.ngxService.start();
    this._applicationService
      .getApplicationVersions(ref)
      .pipe(
        finalize(() => {
          // this.getApplicationFields(this.applicationTypeId);
          this.ngxService.stop();
        })
      )
      .subscribe(
        (data) => {
          console.log(data);
          // this.payload = data.result;
          // // this.payload.id = id;
          // this._applicationDataService.setCreateAppData(this.payload);
        },
        (error) => {
          console.log(error);
          this.ngxService.stop();
        }
      );
  }
}
