import { Component, OnInit, Injector, ChangeDetectionStrategy } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { BehaviorSubject } from 'rxjs';

@Component({
    templateUrl: './profile.component.html',
    animations: [appModuleAnimation()],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileComponent extends AppComponentBase implements OnInit {
    user = {
        title: 'Mr',
        firstName: 'Amodu',
        lastName: 'Tobi',
        organization: 'Facebook',
        position: 'Broker',

    };
    userImage$: BehaviorSubject<string>;
    submitting = false;

    constructor(injector: Injector) {
        super(injector);
        this.userImage$ = new BehaviorSubject('assets/img/user.png');
    }

    ngOnInit(): void {
    }

    updateImage(event) {
        const fileList: FileList = event.target.files;

        if (fileList.length > 0) {
            const file: File = fileList[0];
            const reader = new FileReader();

            reader.addEventListener('load', (e: any) => {
                this.userImage$.next(e.target.result);
            });
            reader.readAsDataURL(file);

        }

    }


}
