import { Component, OnInit, Injector, ChangeDetectionStrategy } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Utils, UtilityService } from '../../shared/helpers/Utils';
import { BehaviorSubject } from 'rxjs';


@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css'],
  animations: [appModuleAnimation()],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MessagesComponent extends AppComponentBase implements OnInit {

  display: Boolean = false;
  displayAssign: Boolean = false;

  messages = [
    {
      id: 1,
      subject: 'Chevron Listing',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
      status: 'read',
    },
    {
      id: 2,
      subject: 'E-Filing Implementation Meeting',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
      status: 'read'
    },
    {
      id: 3,
      subject: 'Review Dangote Application',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
      status: 'unread'
    }
  ];

  selectedMessage$: BehaviorSubject<Object>;
  unread = 0;

  constructor(injector: Injector, public utilityService: UtilityService) {
    super(injector);
    this.selectedMessage$ = new BehaviorSubject({});
  }

  ngOnInit(): void {
    const arr = this.messages.filter(item => item.status === 'unread');
    this.unread = arr.length;
  }

  onSelect(id) {
    this.selectedMessage$.next(this.messages.filter(item => item.id === id)[0]);
  }

  showDialog() {
    this.display = !this.display;
  }

  toggleDialog() {
    this.display = !this.display;
  }

  toggleAssignDialog() {
    this.displayAssign = !this.displayAssign;
  }

}
