import { Component, Output, EventEmitter, AfterViewInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-provide-information',
    templateUrl: './provide-information.component.html',
})
export class ProvideInformationComponent implements AfterViewInit {
    submitting = false;
    currentSection = 'section1';
    screenHeight: any;
    screenWidth: any;
    selectedform: any;
    @Output() openNext = new EventEmitter<any>();

    constructor( ) {
        this.onResize();
    }

    Forms = ['Equity', 'Fixed Income', 'Mutual Funds/REITs/ETF',  'Block Divestment', 'Block Divestment' ,'Delisting'     ];
    
    listing = ['Initial Listing',  'Supplementary Listing', 'Fixed Income' , 'Federal Government Bonds', 'Mutual Funds/REITs/ETF', 'State Government Bonds', 'Delisting', 'Supplementary Application', 'Delisting' ];
  
  
    
      value: Date;


    arr = [
        { label: 'Select City', value: null },
        { label: 'New York', value: { id: 1, name: 'New York', code: 'NY' } },
        { label: 'Rome', value: { id: 2, name: 'Rome', code: 'RM' } },
        { label: 'London', value: { id: 3, name: 'London', code: 'LDN' } },
        { label: 'Istanbul', value: { id: 4, name: 'Istanbul', code: 'IST' } },
        { label: 'Paris', value: { id: 5, name: 'Paris', code: 'PRS' } }
    ];

    @HostListener('window:resize', ['$event'])
    onResize(event?) {
        this.screenHeight = window.innerHeight;
        this.screenWidth = window.innerWidth;
    }


    ngAfterViewInit() {
    }

  

    handleOpenNext() {
        this.openNext.emit();
    }

    onSectionChange(sectionId: string) {
        this.currentSection = sectionId;
    }

    scrollTo(section) {
        document.querySelector('#' + section)
            .scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
    }

}

