import { Component, Injector, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
  PagedListingComponentBase,
  PagedRequestDto
} from 'shared/paged-listing-component-base';
import {
  IssuingCompanyServiceProxy,
  IssuingCompanyDto,
  IssuingCompanyDtoPagedResultDto
} from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { BehaviorSubject, Observable } from 'rxjs';
import { ViewIssuingCompanyDialogComponent } from './view-issuing-company/view-issuing-company-dialog.component';
import { CreateIssuingCompanyDialogComponent } from './create-issuing-company/create-issuing-company-dialog.component';
import { EditIssuingCompanyDialogComponent } from './edit-issuing-company/edit-issuing-company-dialog.component';


class PagedIssuingCompaniesRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  templateUrl: './issuing-companies.component.html',
  animations: [appModuleAnimation()]
})
export class IssuingCompaniesComponent extends PagedListingComponentBase<IssuingCompanyDto> {
  @ViewChild('dt') table: Table;
  issuingCompanies: IssuingCompanyDto[] = [];
  keyword = '';
  isActive: boolean | null;
  advancedFiltersVisible = false;
  selectedIssuingCompany$: BehaviorSubject<IssuingCompanyDto>;

  cols = [
    { field: 'sn', header: 'S/N' },
    { field: 'name', header: 'Company name' },
    { field: 'rcNumber', header: 'Registration Number' },
    { field: 'isActive', header: 'Is active' }
  ];

  buttonItems = [
    {
      label: 'Edit', visible: this.isGranted('EditIssuingCompany'), command: () => {
        this.editIssuingCompany(this.selectedIssuingCompany$.value);
      }
    },
    {
      label: 'Delete', visible: this.isGranted('DeleteIssuingCompany'), command: () => {
        this.delete(this.selectedIssuingCompany$.value);
      }
    },
    {
      label: 'View', visible: this.isGranted('ViewIssuingCompany'), command: () => {
        this.viewIssuingCompany(this.selectedIssuingCompany$.value);
      }
    }
  ];

  constructor(
    injector: Injector,
    private _issuingCompanyService: IssuingCompanyServiceProxy,
    private _modalService: BsModalService
  ) {
    super(injector);

    this.selectedIssuingCompany$ = new BehaviorSubject(new IssuingCompanyDto());
  }

  setSelectedIssuingCompany(issuingCompany: IssuingCompanyDto): void {
    this.selectedIssuingCompany$.next(issuingCompany);
  }

  createIssuingCompany(): void {
    this.showCreateOrEditIssuingCompanyDialog();
  }

  editIssuingCompany(issuingCompany: IssuingCompanyDto): void {
    this.showCreateOrEditIssuingCompanyDialog(issuingCompany.id);
  }

  viewIssuingCompany(issuingCompany: IssuingCompanyDto): void {
    this.showViewIssuingCompanyDialog(issuingCompany.id);
  }

  clearFilters(): void {
    this.keyword = '';
    this.isActive = undefined;
    this.getDataPage(1);
  }

  protected list(
    request: PagedIssuingCompaniesRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    // this._issuingCompanyService.getAllIssuingCompanies(
    //     request.keyword,
    //     request.isActive,
    //     request.skipCount,
    //     request.maxResultCount
    //   ).pipe(
    //     finalize(() => {
    //       finishedCallback();
    //     })
    //   ).subscribe((result: IssuingCompanyDtoPagedResultDto) => {
    //     this.issuingCompanies = result.items;
    //     this.showPaging(result, pageNumber);
    //   });

    this._issuingCompanyService.getMyIssuingCompanies().pipe(
      finalize(() => {
        finishedCallback();
      })
    ).subscribe((result: IssuingCompanyDto[]) => {
      this.issuingCompanies = result;
      //this.showPaging(result, pageNumber);
    });

  }

  protected delete(issuingCompany: IssuingCompanyDto): void {
    abp.message.confirm(
      `${issuingCompany.name} will be deleted`,
      undefined,
      (result: boolean) => {
        if (result) {
          this._issuingCompanyService.deleteIssuingCompany(issuingCompany.id).subscribe(() => {
            abp.notify.success(this.l('SuccessfullyDeleted'));
            this.refresh();
          });
        }
      }
    );
  }

  private showViewIssuingCompanyDialog(id?: number): void {
    const viewIssuingCompanyDialog = this._modalService.show(
      ViewIssuingCompanyDialogComponent,
      {
        backdrop: 'static',
        keyboard: false,
        animated: true,
        ignoreBackdropClick: true,
        class: 'modal-lg',
        initialState: {
          id: id,
        },
      }
    );
  }

  private showCreateOrEditIssuingCompanyDialog(id?: number): void {
    let createOrEditIssuingCompanyDialog: BsModalRef;
    if (!id) {
      createOrEditIssuingCompanyDialog = this._modalService.show(
        CreateIssuingCompanyDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
        }
      );
    } else {
      createOrEditIssuingCompanyDialog = this._modalService.show(
        EditIssuingCompanyDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
          initialState: {
            id: id,
          },
        }
      );
    }

    createOrEditIssuingCompanyDialog.content.onSave.subscribe(() => {
      this.refresh();
    });
  }
}
