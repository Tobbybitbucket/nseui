import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';

@Component({
    selector: 'app-message-dialog',
    templateUrl: './message-dialog.component.html'
})
export class MessageDialogComponent implements OnInit {


    @Input() visible: boolean = false;
    @Output() onClose: EventEmitter<any> = new EventEmitter();

    subject = '';
    message = '';
    recipients: string[];

    constructor() {
    }

    ngOnInit(): void {
    }

    closeModal(): void {
        this.onClose.emit();
      }
}
