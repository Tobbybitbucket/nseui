import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  FeesCalculationServiceProxy,
  FeesCalculationDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './view-fees-calculation-dialog.component.html'
})
export class ViewFeesCalculationDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  feesCalculation = new FeesCalculationDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _feesCalculationService: FeesCalculationServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._feesCalculationService.getFeesCalculation(this.id).subscribe((result) => {
      this.feesCalculation = result.result;
    });
  }
}
