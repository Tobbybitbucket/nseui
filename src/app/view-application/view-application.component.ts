import { Component, Input, OnInit } from "@angular/core";
import {
  ApplicationServiceProxy, ApprovalReviewServiceProxy, ApprovalLogApiResult,
  SystemlogServiceProxy, ApplicationLetterServiceProxy, 
} from "@shared/service-proxies/service-proxies";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { ApplicationDataService } from "../application/services/application-data.service";
import { finalize } from "rxjs/operators";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { jsPDF } from "jspdf";
//import * as jsPDF from 'jspdf';
import html2canvas from "html2canvas";

@Component({
  selector: "app-view-application",
  templateUrl: "./view-application.component.html",
  styleUrls: ["./view-application.component.css"],
})
export class ViewApplicationComponent implements OnInit {
  @Input() appid: number = 0;
  @Input() apptypeid: number = 0;
  @Input() visible: boolean;

  applicationId: number;
  applicationTypeId: number;
  applicationFields: any;
  applicationData: any;
  applicationLogs: any;
  viewApplicationLetters:boolean = false;
  display: boolean = false;
  constructor(
    private _applicationService: ApplicationServiceProxy,
    private _systemLogService: SystemlogServiceProxy,
    private _applicationDataService: ApplicationDataService,
    private _applicationLetterServiceProxy: ApplicationLetterServiceProxy,
    private _approvalReviewServiceProxy: ApprovalReviewServiceProxy,
    private route: ActivatedRoute,
    private router: Router,
    private ngxService: NgxUiLoaderService
  ) {}
  getApplicationFields(appTypeId: number) {
    this.ngxService.start();
    this._applicationDataService.appDataState.subscribe(
      (data) => {
        this.applicationFields = data.result.filter(
          (type) => type.id === appTypeId
        )[0].sections;
        console.log(this.applicationFields);
      },
      (error) => {
        console.log(error);
        this.ngxService.stop();
      }
    );
  }
  getApplicationData(id: number) {
    this.ngxService.start();
    this._applicationService
      .getApplication(id)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(
        (data) => {
          console.log(data.result);
          this.applicationData = data.result;
          this.getApplicationFields(this.applicationTypeId);
        },
        (error) => {
          console.log(error);
          this.ngxService.stop();
        }
      );
  }
  getLogs(id: number) {
    this.ngxService.start();
    this._systemLogService
      .fetchLogs(id)
      .pipe(
        finalize(() => {
          // this.getApplicationFields(this.applicationTypeId);
          this.ngxService.stop();
        })
      )
      .subscribe(
        (data) => {
          console.log(data);
          this.applicationLogs = data;
        },
        (error) => {
          console.log(error);
          this.ngxService.stop();
        }
      );
  }
  returnToDashboard() {
    this.router.navigate([`/app/home`]);
  }
  isObject(val) {
    return typeof val === "object";
  }
  ngOnInit(): void {
    this.viewApplicationLetters = false;
    if (this.appid > 0) {
      this.applicationId = this.appid;
    } else {
      this.applicationId = Number(
        this.route.snapshot.queryParamMap.get("appId")
      );
    }

    if (this.apptypeid > 0) {
      this.applicationTypeId = this.apptypeid;
    } else {
      this.applicationTypeId = Number(
        this.route.snapshot.queryParamMap.get("appTypeId")
      );
    }

    this.getApplicationData(this.applicationId);
    this.getLogs(this.applicationId);

    
    this.getApplicationLetters(this.applicationId); 

  }

  ApprovalLetters:any;
  getApplicationLetters(applicationId: number) {
    this._applicationLetterServiceProxy
      .getApplicationLetter(applicationId, 0)
      .pipe(
        finalize(() => {
         // this.ngxService.stop(); 
        })
      )
      .subscribe((retval) => {
        console.log(retval, "DONEEE");
        this.ApprovalLetters = retval.result;
      });
  }

  
  downloadAppLetterModal: boolean;
  selectedApplicationLetter: any;
  downloadApplicationLetter(applicationLetter) {
    this.downloadAppLetterModal = true;
    this.viewApplicationLetters = false;
    this.selectedApplicationLetter = applicationLetter;
  }

  getPdf(applicationLetter) {
    html2canvas(document.getElementById('template_editor'+applicationLetter.id)).then((canvas) => {
      var pdf = new jsPDF("p", "pt", [canvas.width, canvas.height]);

      var imgData = canvas.toDataURL("image/jpeg", 1.0);
      pdf.addImage(imgData,0,0,canvas.width, canvas.height);
      pdf.save(applicationLetter.title+".pdf");
    });
  }

}
