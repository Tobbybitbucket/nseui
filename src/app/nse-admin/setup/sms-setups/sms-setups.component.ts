import { Component, Injector, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
  PagedListingComponentBase,
  PagedRequestDto
} from 'shared/paged-listing-component-base';
import {
  SmsSetupServiceProxy,
  SmsSetupDto,
  SmsSetupDtoPagedResultDto
} from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { BehaviorSubject } from 'rxjs';
import { ViewSmsSetupDialogComponent } from './view-sms-setup/view-sms-setup-dialog.component';
import { CreateSmsSetupDialogComponent } from './create-sms-setup/create-sms-setup-dialog.component';
import { EditSmsSetupDialogComponent } from './edit-sms-setup/edit-sms-setup-dialog.component';


class PagedSmsSetupsRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  templateUrl: './sms-setups.component.html',
  animations: [appModuleAnimation()]
})
export class SmsSetupsComponent extends PagedListingComponentBase<SmsSetupDto> {
  @ViewChild('dt') table: Table;
  smsSetups: SmsSetupDto[] = [];
  keyword = '';
  isActive: boolean | null;
  advancedFiltersVisible = false;
  selectedSmsSetup$: BehaviorSubject<SmsSetupDto>;

  cols = [
    { field: 'sn', header: 'S/N' },
    { field: 'baseUrl', header: 'BaseUrl' },
    { field: 'senderName', header: 'Sender Name' },
    { field: 'userName', header: 'User Name' },
    { field: 'password', header: 'Password' }
  ];

  buttonItems = [
    {
      label: 'Edit', visible: this.isGranted('EditSmsSetup'), command: () => {
        this.editSmsSetup(this.selectedSmsSetup$.value);
      }
    },
    {
      label: 'Delete', visible: this.isGranted('DeleteSmsSetup'), command: () => {
        this.delete(this.selectedSmsSetup$.value);
      }
    },
    {
      label: 'View', visible: this.isGranted('ViewSmsSetup'), command: () => {
        this.viewSmsSetup(this.selectedSmsSetup$.value);
      }
    }
  ];

  constructor(
    injector: Injector,
    private _smsSetupService: SmsSetupServiceProxy,
    private _modalService: BsModalService
  ) {
    super(injector);

    this.selectedSmsSetup$ = new BehaviorSubject(new SmsSetupDto());
  }

  setSelectedSmsSetup(smsSetup: SmsSetupDto): void {
    this.selectedSmsSetup$.next(smsSetup);
  }

  createSmsSetup(): void {
    this.showCreateOrEditSmsSetupDialog();
  }

  editSmsSetup(smsSetup: SmsSetupDto): void {
    this.showCreateOrEditSmsSetupDialog(smsSetup.id);
  }

  viewSmsSetup(smsSetup: SmsSetupDto): void {
    this.showViewSmsSetupDialog(smsSetup.id);
  }

  clearFilters(): void {
    this.keyword = '';
    this.isActive = undefined;
    this.getDataPage(1);
  }

  protected list(
    request: PagedSmsSetupsRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    this._smsSetupService
      .getAllSmsSetups(
        request.keyword,
        request.isActive,
        request.skipCount,
        request.maxResultCount
      )
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: SmsSetupDtoPagedResultDto) => {
        this.smsSetups = result.items;
        this.showPaging(result, pageNumber);
      });
  }

  protected delete(smsSetup: SmsSetupDto): void {
    abp.message.confirm(
      `${smsSetup.baseUrl} will be deleted`,
      undefined,
      (result: boolean) => {
        if (result) {
          this._smsSetupService.deleteSmsSetup(smsSetup.id).subscribe(() => {
            abp.notify.success(this.l('SuccessfullyDeleted'));
            this.refresh();
          });
        }
      }
    );
  }

  private showViewSmsSetupDialog(id?: number): void {
    const viewSmsSetupDialog = this._modalService.show(
      ViewSmsSetupDialogComponent,
      {
        backdrop: 'static',
        keyboard: false,
        animated: true,
        ignoreBackdropClick: true,
        class: 'modal-lg',
        initialState: {
          id: id,
        },
      }
    );
  }

  private showCreateOrEditSmsSetupDialog(id?: number): void {
    let createOrEditSmsSetupDialog: BsModalRef;
    if (!id) {
      createOrEditSmsSetupDialog = this._modalService.show(
        CreateSmsSetupDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
        }
      );
    } else {
      createOrEditSmsSetupDialog = this._modalService.show(
        EditSmsSetupDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
          initialState: {
            id: id,
          },
        }
      );
    }

    createOrEditSmsSetupDialog.content.onSave.subscribe(() => {
      this.refresh();
    });
  }
}
