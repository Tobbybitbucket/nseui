export class MenuItem {
  id: number;
  parentId: number;
  label: string;
  route: string;
  icon: string;
  permissionName: string;
  isActive?: boolean;
  isCollapsed?: boolean;
  children: MenuItem[];
  count: number;

  constructor(
    label: string,
    route: string,
    icon: string,
    permissionName: string = null,
    children: MenuItem[] = null,
    count?: number
  ) {
    this.label = label;
    this.route = route;
    this.icon = icon;
    this.permissionName = permissionName;
    this.children = children;
    this.count = count;
  }
}
