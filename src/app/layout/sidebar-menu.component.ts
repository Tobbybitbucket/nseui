import { Component, Injector, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import {
  Router,
  RouterEvent,
  NavigationEnd,
  PRIMARY_OUTLET,
} from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { MenuItem } from '@shared/layout/menu-item';

@Component({
  selector: 'sidebar-menu',
  templateUrl: './sidebar-menu.component.html',
})
export class SidebarMenuComponent extends AppComponentBase implements OnInit {
  menuItems: MenuItem[];
  menuItemsMap: { [key: number]: MenuItem } = {};
  activatedMenuItems: MenuItem[] = [];
  routerEvents: BehaviorSubject<RouterEvent> = new BehaviorSubject(undefined);
  homeRoute = '/app/home';

  constructor(injector: Injector, private router: Router) {
    super(injector);
    this.router.events.subscribe(this.routerEvents);
  }

  ngOnInit(): void {
    this.menuItems = this.getMenuItems();
    this.patchMenuItems(this.menuItems);
    this.routerEvents
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((event) => {
        const currentUrl = event.url !== '/' ? event.url : this.homeRoute;
        const primaryUrlSegmentGroup = this.router.parseUrl(currentUrl).root
          .children[PRIMARY_OUTLET];
        if (primaryUrlSegmentGroup) {
          this.activateMenuItems('/' + primaryUrlSegmentGroup.toString());
        }
      });
  }

  getMenuItems(): MenuItem[] {
    return [
      new MenuItem(this.l('HomePage'), '/app/home', 'pi pi-home'),
      new MenuItem(
        this.l('Tenants'),
        '/app/te  nants',
        'fas fa-building',
        'Pages.Tenants'
      ),
      new MenuItem(
        this.l('Users'),
        '/app/users',
        'fas fa-users',
        'Pages.Users'
      ),
      new MenuItem(
        this.l('Roles'),
        '/app/roles',
        'fas fa-theater-masks',
        'Pages.Roles'
      ),
      new MenuItem(this.l('Pre-approval'), '', 'far fa-file-alt', '', [
        new MenuItem('New Application', '/app/new-application', ''),
        new MenuItem('Resume Application', '/app/resume-application', ''),
      ]),
      new MenuItem(this.l('Steps'), '', 'fas fa-info-circle', '', [
        new MenuItem(this.l('Step One'), '', '', '', [
          new MenuItem('Submenu One', '/app/resume-application', '')
        ]),
        new MenuItem(this.l('Step One'), '', '', '', [
          new MenuItem('Submenu One', '/app/resume-application', ''),
          new MenuItem('Submenu Two', '/app/resume-application', '')
        ]),
        
      ]),
      new MenuItem(this.l('My Workspace'), '', 'fas fa-th-large', '', [
        new MenuItem(
          'Equity Application',
          '/app/equity-application',
          '',
          null,
          null,
          4
        ),
        new MenuItem('State Bonds', '/app/state-bonds', ''),
        new MenuItem('Payment Review', '/app/corporate-bonds', ''),
      ]),
      new MenuItem(this.l('Messages'), '/app/messages', 'far fa-envelope'),
      new MenuItem(this.l('Profile'), '/app/profile', 'far fa-user-circle'),
      new MenuItem(this.l('Contact Us'), '/app/contact', 'fas fa-phone-alt'),
      new MenuItem(this.l('Configuration'), '/app/settings', 'fas fa-cog'),
      new MenuItem('Help', '', 'far fa-file-alt', '', [
        new MenuItem('FAQ', '/app/faq', ''),
        new MenuItem('User Guides', '/app/userguides', ''),
      ]),
    ];
  }

  patchMenuItems(items: MenuItem[], parentId?: number): void {
    items.forEach((item: MenuItem, index: number) => {
      item.id = parentId ? Number(parentId + '' + (index + 1)) : index + 1;
      if (parentId) {
        item.parentId = parentId;
      }
      if (parentId || item.children) {
        this.menuItemsMap[item.id] = item;
      }
      if (item.children) {
        this.patchMenuItems(item.children, item.id);
      }
    });
  }

  activateMenuItems(url: string): void {
    this.deactivateMenuItems(this.menuItems);
    this.activatedMenuItems = [];
    const foundedItems = this.findMenuItemsByUrl(url, this.menuItems);
    foundedItems.forEach((item) => {
      this.activateMenuItem(item);
    });
  }

  deactivateMenuItems(items: MenuItem[]): void {
    items.forEach((item: MenuItem) => {
      item.isActive = false;
      item.isCollapsed = true;
      if (item.children) {
        this.deactivateMenuItems(item.children);
      }
    });
  }

  findMenuItemsByUrl(
    url: string,
    items: MenuItem[],
    foundedItems: MenuItem[] = []
  ): MenuItem[] {
    items.forEach((item: MenuItem) => {
      if (item.route === url) {
        foundedItems.push(item);
      } else if (item.children) {
        this.findMenuItemsByUrl(url, item.children, foundedItems);
      }
    });
    return foundedItems;
  }

  activateMenuItem(item: MenuItem): void {
    item.isActive = true;
    if (item.children) {
      item.isCollapsed = false;
    }
    this.activatedMenuItems.push(item);
    if (item.parentId) {
      this.activateMenuItem(this.menuItemsMap[item.parentId]);
    }
  }

  isMenuItemVisible(item: MenuItem): boolean {
    if (!item.permissionName) {
      return true;
    }
    return this.permission.isGranted(item.permissionName);
  }
}
