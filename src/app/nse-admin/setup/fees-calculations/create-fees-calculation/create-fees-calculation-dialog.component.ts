import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  FeesCalculationServiceProxy,
  CreateFeesCalculationDto
} from '@shared/service-proxies/service-proxies';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';

@Component({
  templateUrl: './create-fees-calculation-dialog.component.html'
})
export class CreateFeesCalculationDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  feesCalculation = new CreateFeesCalculationDto();

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _feesCalculationService: FeesCalculationServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.feesCalculation.isActive = true;
  }

  save(): void {
    this.saving = true;
    this._feesCalculationService
      .createFeesCalculation(this.feesCalculation)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
