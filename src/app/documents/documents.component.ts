import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import {
  ApplicationServiceProxy,
  DocLogServiceProxy,
  DocumentUploadModel,
  IDocumentUploadModel,
  IUploadDocModel,
} from "@shared/service-proxies/service-proxies";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { finalize } from "rxjs/operators";

@Component({
  selector: "app-documents",
  templateUrl: "./documents.component.html",
  styleUrls: ["./documents.component.css"],
})
export class DocumentsComponent implements OnInit {
  submitting = false;
  applicationId: number;
  applicationData: any;
  appVersionRef: string;
  documents: any = [];
  constructor(
    private ngxService: NgxUiLoaderService,
    private _docLogService: DocLogServiceProxy,
    private route: ActivatedRoute,
    private router: Router,
    private _applicationService: ApplicationServiceProxy
  ) {}
  loading = false;
  requiredDocuments: DocumentUploadModel | any = [{ name: "Document" }];
  // documents: IDocumentUploadModel;
  fileObject: IUploadDocModel;
  fileObjectList: Array<IUploadDocModel> = [];
  cols = [
    { field: "id", header: "S/N" },
    { field: "name", header: "Document Name" },
    { field: "document", header: "Document Uploaded" },
    { field: "action", header: "" },
  ];

  ngOnInit(): void {
    this.applicationId = +this.route.snapshot.queryParamMap.get("appId");
    this.getApplication(this.applicationId);
  }

  getAppDocs(appId) {
    this.ngxService.start();
    this._docLogService
      .fetchApplicationDocs(appId)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(
        (res) => {
          this.documents = res.result;
          console.log(this.documents);
        },
        (error) => {
          console.log(error);
          this.ngxService.stop();
        }
      );
  }
  getApplication(appId) {
    if (appId) {
      this.ngxService.start();
      this._applicationService
        .getApplication(appId)
        .pipe(
          finalize(() => {
            this.ngxService.stop();
          })
        )
        .subscribe(
          (data) => {
            console.log(data);
            this.applicationData = data.result;
            this.appVersionRef = this.applicationData.appVersionRef;
            this.getAppDocs(this.appVersionRef);
          },
          (error) => {
            console.log(error);
            this.ngxService.stop();
          }
        );
    }
  }

  fileChangeEvent(files: FileList, doc) {
    let me = this;
    let file = files[0];
    let ext = file.name.split(".").pop();
    let reader = new FileReader();
    let applicationNo = this.appVersionRef;
    reader.readAsDataURL(file);
    reader.onload = function () {
      me.ngxService.start();
      me.fileObject = {
        fileBase64String: reader.result
          .toString()
          .substring(reader.result.toString().lastIndexOf(",") + 1),
        fileName: doc.name,
        fileExt: ext,
        fileType: "APPLICATION_FORM",
        applicationNo: applicationNo,
        is_post_approval: true,
      };
      // me.documents = {
      //   id: doc.id,
      //   name: doc.name,
      //   document: "",
      //   file: "",
      // };
    };
    setTimeout(() => {
      var obj = this.fileObjectList.find(
        (element) => element.fileName === this.fileObject.fileName
      );

      if (typeof obj == "object") {
        this.fileObjectList.forEach((element, index) => {
          if (element.fileName === this.fileObject.fileName) {
            this.fileObjectList[index] = this.fileObject;
          }
        });
      } else if (typeof obj == "undefined") {
        this.fileObjectList.push(me.fileObject);
      }
      me.ngxService.stop();
    }, 3000);
    reader.onerror = function (error) {
      console.log("Error: ", error);
    };
  }
  handlePrev() {
    this.router.navigate([`/app/home`]);
  }
  // addNewDoc() {
  //   this.requiredDocuments.push({
  //     name: `Document ` + (this.requiredDocuments.length + 1),
  //   });
  // }
  // removeDoc(i) {
  //   this.requiredDocuments.splice(i, 1);
  // }
  processDocument() {
    console.log(this.fileObjectList);
    if (this.fileObjectList.length !== 0) {
      this.fileObjectList.forEach((element, index) => {
        this.ngxService.start();
        this.uploadDocument(element, index);
      });
    } else {
      // this.handleOpenNext();
    }
  }

  public uploadDocument(data, index): void {
    this.ngxService.start();
    this._docLogService
      .uploadDoc(data)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(
        (res) => {
          console.log(res.result.message, res);
          this.handlePrev();
        },
        (error) => {
          console.log(error);
          this.ngxService.stop();
        }
      );
  }
}
