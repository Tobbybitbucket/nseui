import { Component, Injector, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
  PagedListingComponentBase,
  PagedRequestDto
} from 'shared/paged-listing-component-base';
import {
  StateServiceProxy,
  StateDto,
  StateDtoPagedResultDto
} from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { BehaviorSubject } from 'rxjs';
import { ViewStateDialogComponent } from './view-state/view-state-dialog.component';
import { CreateStateDialogComponent } from './create-state/create-state-dialog.component';
import { EditStateDialogComponent } from './edit-state/edit-state-dialog.component';


class PagedStatesRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  templateUrl: './states.component.html',
  animations: [appModuleAnimation()]
})
export class StatesComponent extends PagedListingComponentBase<StateDto> {
  @ViewChild('dt') table: Table;
  states: StateDto[] = [];
  keyword = '';
  isActive: boolean | null;
  advancedFiltersVisible = false;
  selectedState$: BehaviorSubject<StateDto>;

  cols = [
    { field: 'sn', header: 'S/N' },
    { field: 'name', header: 'State name' },
    { field: 'country', header: 'Country' },
    { field: 'isActive', header: 'Is active' }
  ];

  buttonItems = [
    {
      label: 'Edit', visible: this.isGranted('EditState'), command: () => {
        this.editState(this.selectedState$.value);
      }
    },
    {
      label: 'Delete', visible: this.isGranted('DeleteState'), command: () => {
        this.delete(this.selectedState$.value);
      }
    },
    {
      label: 'View', visible: this.isGranted('ViewState'), command: () => {
        this.viewState(this.selectedState$.value);
      }
    }
  ];

  constructor(
    injector: Injector,
    private _stateService: StateServiceProxy,
    private _modalService: BsModalService
  ) {
    super(injector);

    this.selectedState$ = new BehaviorSubject(new StateDto());
  }

  setSelectedState(state: StateDto): void {
    this.selectedState$.next(state);
  }

  createState(): void {
    this.showCreateOrEditStateDialog();
  }

  editState(state: StateDto): void {
    this.showCreateOrEditStateDialog(state.id);
  }

  viewState(state: StateDto): void {
    this.showViewStateDialog(state.id);
  }

  clearFilters(): void {
    this.keyword = '';
    this.isActive = undefined;
    this.getDataPage(1);
  }

  protected list(
    request: PagedStatesRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    this._stateService
      .getAllStates(
        request.keyword,
        request.isActive,
        request.skipCount,
        request.maxResultCount
      )
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: StateDtoPagedResultDto) => {
        this.states = result.items;
        this.showPaging(result, pageNumber);
      });
  }

  protected delete(state: StateDto): void {
    abp.message.confirm(
      `${state.name} will be deleted`,
      undefined,
      (result: boolean) => {
        if (result) {
          this._stateService.deleteState(state.id).subscribe(() => {
            abp.notify.success(this.l('SuccessfullyDeleted'));
            this.refresh();
          });
        }
      }
    );
  }

  private showViewStateDialog(id?: number): void {
    const viewStateDialog = this._modalService.show(
      ViewStateDialogComponent,
      {
        backdrop: 'static',
        keyboard: false,
        animated: true,
        ignoreBackdropClick: true,
        class: 'modal-lg',
        initialState: {
          id: id,
        },
      }
    );
  }

  private showCreateOrEditStateDialog(id?: number): void {
    let createOrEditStateDialog: BsModalRef;
    if (!id) {
      createOrEditStateDialog = this._modalService.show(
        CreateStateDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
        }
      );
    } else {
      createOrEditStateDialog = this._modalService.show(
        EditStateDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
          initialState: {
            id: id,
          },
        }
      );
    }

    createOrEditStateDialog.content.onSave.subscribe(() => {
      this.refresh();
    });
  }
}
