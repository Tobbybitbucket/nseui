import { Component, OnInit, Input ,Output, EventEmitter} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-application-dialog',
  templateUrl: './application-dialog.component.html',
  styleUrls: ['./application-dialog.component.css']
})
export class ApplicationDialogComponent implements OnInit {

  @Input() visible: boolean = false;
  @Output() onClose: EventEmitter<any> = new EventEmitter();

  selectedValue = {};
  selectedValue2 = {};

  arr = [
    { label: '-- Choose --', value: null },
    { label: 'Equity', value: { id: 1, name: 'Equity' } },
    { label: 'Fixed Income', value: { id: 2, name: 'Fixed Income' } },
    { label: 'Mutual Funds/REITs/ETF', value: { id: 3, name: 'Mutual Funds/REITs/ETF'} },
    { label: 'Block Divestment', value: { id: 4, name: 'Block Divestment' } },
    { label: 'Delisting', value: { id: 5, name: 'Delisting' } }
  ];

  arr2 = [
    { label: '-- Choose --', value: null },
    { label: 'Initial Listing', value: { id: 1, name: 'Equity' } },
    { label: 'Supplementary Listing', value: { id: 2, name: 'Fixed Income' } },
    { label: 'Federal Government Bonds', value: { id: 3, name: 'Mutual Funds/REITs/ETF'} },
    { label: 'State Government Bonds', value: { id: 4, name: 'Block Divestment' } },
    { label: 'New', value: { id: 5, name: 'Delisting' } },
    { label: 'Supplementary Application', value: { id: 6, name: 'Delisting' } }
   
  ];
  constructor( private router: Router) {
  
}

  ngOnInit(): void {
  }

  showDialog() {
    this.router.navigate(['/app/new-application']);
    }
    
    closeModal(): void {
      this.onClose.emit();
    }

}
