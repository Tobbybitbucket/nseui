import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

// interface City {
//   name: string;
//   code: string
// }

@Component({
  selector: 'app-assign-application-dialog',
  templateUrl: './assign-application-dialog.component.html',
  styleUrls: ['./assign-application-dialog.component.css']
})
export class AssignApplicationDialogComponent implements OnInit {

  @Input() visible: boolean;
  @Output() onClose: EventEmitter<any> = new EventEmitter();


  selectedValue = {};

  arr = [
    { label: 'Select officer', value: null },
    { label: 'Tobi', value: { id: 1, name: 'Tobi', code: 'TB' } },
    { label: 'Miracle', value: { id: 2, name: 'Miracle', code: 'MR' } },
    { label: 'Francis', value: { id: 3, name: 'Francis', code: 'FR' } },
    { label: 'William', value: { id: 4, name: 'William', code: 'WM' } },
    { label: 'Austin', value: { id: 5, name: 'Austin', code: 'AU' } }
  ];

  constructor() {
  }

  ngOnInit(): void {
  }

  closeModal(): void {
    this.onClose.emit();
  }
}
