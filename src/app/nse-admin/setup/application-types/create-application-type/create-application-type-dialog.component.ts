import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  ApplicationTypeServiceProxy,
  CreateApplicationTypeDto
} from '@shared/service-proxies/service-proxies';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';

@Component({
  templateUrl: './create-application-type-dialog.component.html'
})
export class CreateApplicationTypeDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  applicationType = new CreateApplicationTypeDto();

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _applicationTypeService: ApplicationTypeServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.applicationType.isActive = true;
  }

  save(): void {
    this.saving = true;
    this._applicationTypeService
      .createApplicationType(this.applicationType)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
