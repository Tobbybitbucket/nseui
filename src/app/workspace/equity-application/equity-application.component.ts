import { Component, OnInit, Injector, ChangeDetectionStrategy, ViewChild, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    templateUrl: './equity-application.component.html',
    animations: [appModuleAnimation()],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EquityApplicationComponent extends AppComponentBase implements OnInit {
    form = {
        field1: 'Lorem Ipsum',
        field2: 'Lorem Ipsum',
        field3: 'Lorem Ipsum',
        field4: 'Lorem Ipsum',

    };
    submitting = false;
    index = 0;
    showComments = false;
    currentSection = 'section1';
    screenHeight: any;
    screenWidth: any;
    selectedValue: string;

    arr = [
        { label: 'Select Action', value: null },
        { label: 'Appraisal Report/ Deficiency letter', value: 'Appraisal Report/ Deficiency letter' },
        { label: 'Appraisal Report/ Deficiency letter', value: 'Appraisal Report/ Deficiency letter' },
    ];

    text: string;

    constructor(injector: Injector) {
        super(injector);
        this.onResize();
    }

    @HostListener('window:resize', ['$event'])
    onResize(event?) {
        this.screenHeight = window.innerHeight;
        this.screenWidth = window.innerWidth;
    }

    ngOnInit(): void {
    }

    openNext() {
        this.index = (this.index === 2) ? 0 : this.index + 1;
    }

    openPrev() {
        this.index = (this.index === 0) ? 2 : this.index - 1;
    }

    toggleSidebar() {
        this.showComments = !this.showComments;
        console.log(this.showComments);
    }

    onSectionChange(sectionId: string) {
        this.currentSection = sectionId;
    }

    scrollTo(section) {
        document.querySelector('#' + section)
            .scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
    }

}