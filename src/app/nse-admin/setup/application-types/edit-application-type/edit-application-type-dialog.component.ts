import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  ApplicationTypeServiceProxy,
  ApplicationTypeDto, CreateApplicationTypeDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './edit-application-type-dialog.component.html'
})
export class EditApplicationTypeDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  applicationType = new CreateApplicationTypeDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _applicationTypeService: ApplicationTypeServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._applicationTypeService.getApplicationType(this.id).subscribe((result) => {
      this.applicationType = result.result;
    });
  }

  save(): void {
    this.saving = true;

    this._applicationTypeService
      .editApplicationType(this.applicationType)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
