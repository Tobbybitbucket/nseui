import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientJsonpModule } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgxPaginationModule } from 'ngx-pagination';
import { AppRoutingModule } from './app-routing.module';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { CardModule } from 'primeng/card';
import { TableModule } from 'primeng/table';
import { SplitButtonModule } from 'primeng/splitbutton';
import { ButtonModule } from 'primeng/button';
import { AccordionModule } from 'primeng/accordion';
import { DialogModule } from 'primeng/dialog';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { EditorModule } from 'primeng/editor';
import { DropdownModule } from 'primeng/dropdown';
import { TabViewModule } from 'primeng/tabview';
import { InputTextModule } from 'primeng/inputtext';
import { FileUploadModule } from 'primeng/fileupload';
import { CalendarModule } from 'primeng/calendar';
import { SidebarModule } from 'primeng/sidebar';
import { MultiSelectModule } from 'primeng/multiselect';
import { ScrollSpyDirective } from '../shared/directives/scrollspy.directive';
import { SelectButtonModule } from 'primeng/selectbutton';
import { InputSwitchModule } from 'primeng/inputswitch';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputMaskModule } from 'primeng/inputmask';
import { ChartModule } from 'primeng/chart';
import { ChipsModule } from 'primeng/chips';
import { SliderModule } from 'primeng/slider';
import { CheckboxModule } from 'primeng/checkbox';
import { AppComponent } from './app.component';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { SharedModule } from '@shared/shared.module';
import { HomeComponent } from '@app/home/home.component';
import { AboutComponent } from '@app/about/about.component';
import { DocumentChecklistComponent } from './application/document-checklist/document-checklist.component';

// tenants
import { TenantsComponent } from '@app/tenants/tenants.component';
import { CreateTenantDialogComponent } from './tenants/create-tenant/create-tenant-dialog.component';
import { EditTenantDialogComponent } from './tenants/edit-tenant/edit-tenant-dialog.component';
// roles
import { RolesComponent } from '@app/roles/roles.component';
import { CreateRoleDialogComponent } from './roles/create-role/create-role-dialog.component';
import { EditRoleDialogComponent } from './roles/edit-role/edit-role-dialog.component';
// users
import { UsersComponent } from '@app/users/users.component';
import { CreateUserDialogComponent } from '@app/users/create-user/create-user-dialog.component';
import { EditUserDialogComponent } from '@app/users/edit-user/edit-user-dialog.component';
import { ChangePasswordComponent } from './users/change-password/change-password.component';
import { ResetPasswordDialogComponent } from './users/reset-password/reset-password.component';
// layout
import { HeaderComponent } from './layout/header.component';
import { HeaderModalComponent } from './layout/header-modal.component';
import { HeaderLeftNavbarComponent } from './layout/header-left-navbar.component';
import { HeaderLanguageMenuComponent } from './layout/header-language-menu.component';
import { HeaderUserMenuComponent } from './layout/header-user-menu.component';
import { FooterComponent } from './layout/footer.component';
import { SidebarComponent } from './layout/sidebar.component';
import { SidebarLogoComponent } from './layout/sidebar-logo.component';
import { SidebarUserPanelComponent } from './layout/sidebar-user-panel.component';
import { SidebarMenuComponent } from './layout/sidebar-menu.component';
import { FaqComponent } from './faq/faq.component';
import { MessagesComponent } from './messages/messages.component';
import { UserGuidesComponent } from './user-guides/user-guides.component';
import { ApplicationComponent } from './application/application.component';
import { ApplicationDialogComponent } from './application/application-dialog/application-dialog.component';
import { AssignApplicationDialogComponent } from './application/assign-application-dialog/assign-application-dialog.component';
import { ProfileComponent } from './profile/profile.component';
import { SettingsComponent } from './settings/settings.component';
import { EquityApplicationComponent } from './workspace/equity-application/equity-application.component';
import { CorporateBondsComponent } from './workspace/corporate-bonds/corporate-bonds.component';
import { StateBondsComponent } from './workspace/state-bonds/state-bonds.component';
import { ProvideInformationComponent } from './application/provide-information/provide-information.component';
import { UploadDocumentationComponent } from './application/upload-documentation/upload-documentation.component';
import { ReviewComponent } from './application/review/review.component';
import { CommentsComponent } from './workspace/comments/comments.component';
import { EmptyStateComponent } from '../shared/components/empty-state/empty-state.component';
import { MessageDialogComponent } from './messages/message-dialog/message-dialog.component';
import { UtilityService } from '@shared/helpers/Utils';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    FaqComponent,
    ProfileComponent,
    SettingsComponent,
    UserGuidesComponent,
    // tenants
    TenantsComponent,
    CreateTenantDialogComponent,
    EditTenantDialogComponent,
    // roles
    RolesComponent,
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    // users
    UsersComponent,
    CreateUserDialogComponent,
    EditUserDialogComponent,
    DocumentChecklistComponent,
    ChangePasswordComponent,
    ResetPasswordDialogComponent,
    // application
    ApplicationComponent,
    ProvideInformationComponent,
    ApplicationDialogComponent,
    AssignApplicationDialogComponent,
    EquityApplicationComponent,
    CorporateBondsComponent,
    StateBondsComponent,
    UploadDocumentationComponent,
    ReviewComponent,
    MessagesComponent,
    CommentsComponent,
    EmptyStateComponent,
    // layout
    HeaderComponent,
    HeaderModalComponent,
    HeaderLeftNavbarComponent,
    HeaderLanguageMenuComponent,
    HeaderUserMenuComponent,
    FooterComponent,
    SidebarComponent,
    SidebarLogoComponent,
    SidebarUserPanelComponent,
    SidebarMenuComponent,
    ScrollSpyDirective,
    MessageDialogComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    ModalModule.forChild(),
    BsDropdownModule,
    CollapseModule,
    TabsModule,
    ToggleButtonModule,
    AppRoutingModule,
    ServiceProxyModule,
    SharedModule,
    NgxPaginationModule,
    CardModule,
    TableModule,
    SplitButtonModule,
    ButtonModule,
    AccordionModule,
    DialogModule,
    ProgressSpinnerModule,
    MultiSelectModule,
    DropdownModule,
    TabViewModule,
    InputTextModule,
    FileUploadModule,
    EditorModule,
    CalendarModule,
    SidebarModule,
    SelectButtonModule,
    InputSwitchModule,
    RadioButtonModule,
    SliderModule,
    InputTextareaModule,
    InputNumberModule,
    InputMaskModule,
    CheckboxModule,
    ChipsModule,
    ChartModule
  ],
  providers: [UtilityService],
  entryComponents: [
    // tenants
    CreateTenantDialogComponent,
    EditTenantDialogComponent,
    // roles
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    // users
    CreateUserDialogComponent,
    EditUserDialogComponent,
    ResetPasswordDialogComponent,
    // application
    ApplicationDialogComponent,
    CommentsComponent,
    EmptyStateComponent,
    MessageDialogComponent
  ],
})
export class AppModule { }
