import { MessageOut } from './../../shared/service-proxies/workflow-service-proxy.service';
import { EmailMessageDto, EmailMessageServiceProxy } from './../../shared/service-proxies/service-proxies';
import { Component, OnInit, Injector, ChangeDetectionStrategy } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { UtilityService } from '../../shared/helpers/Utils';
import { BehaviorSubject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ReplyEmailMessageDialogComponent } from './reply-email-message-dialog/reply-email-message-dialog.component';
import { CreateEmailMessageDialogComponent } from './create-email-message-dialog/create-email-message-dialog.component';
import { ForwardEmailMessageDialogComponent } from './forward-email-message-dialog/forward-email-message-dialog.component';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-email-messages',
  templateUrl: './email-messages.component.html',
  styleUrls: ['./email-messages.component.css'],
  animations: [appModuleAnimation()],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmailMessagesComponent extends AppComponentBase implements OnInit {
  display: boolean;
  emailMessages: EmailMessageDto[] = [];

  selectedEmailMessage$: BehaviorSubject<EmailMessageDto>;
  unread = 0;

  constructor(injector: Injector,
    private _emailMessageService: EmailMessageServiceProxy,
    private _modalService: BsModalService,
    private router: Router,
    private route: ActivatedRoute,
    public utilityService: UtilityService) {
    super(injector);
    this.selectedEmailMessage$ = new BehaviorSubject(new EmailMessageDto());
  }

  ngOnInit(): void {
    this.emailMessages = this.route.snapshot.data.data;
    this.SetUnreadCount();
  }
  SetUnreadCount() {
    const arr = this.emailMessages.filter(item => !item.read);
    this.unread = arr.length;
  }

  onSelect(id) {
    this.selectedEmailMessage$.next(this.emailMessages.filter(item => item.id === id)[0]);
    this._emailMessageService
      .read(id)
      .pipe(
        finalize(() => {
          this.emailMessages.filter(item => item.id === id)[0].read = true;
          this.GetMessages();
        })
      )
      .subscribe((x: MessageOut) => {
      });
  }

  createEmailMessage(): void {
    this.showCreateOrForwardEmailMessageDialog();
  }

  forwardEmailMessage(emailMessage): void {
    this.showCreateOrForwardEmailMessageDialog(emailMessage.value.id);
  }

  replyEmailMessage(emailMessage): void {
    this.showReplyEmailMessageDialog(emailMessage.value.id);
  }

  GetMessages(): void {
    this._emailMessageService
      .getAllEmailMessages()
      .pipe(
        finalize(() => {

        })
      )
      .subscribe((result: EmailMessageDto[]) => {
        this.emailMessages = result;
        this.SetUnreadCount();
      });
  }

  protected delete(emailMessage: EmailMessageDto): void {
    abp.message.confirm(
      `${emailMessage.subject} will be deleted`,
      undefined,
      (result: boolean) => {
        if (result) {
          this._emailMessageService.deleteEmailMessage(emailMessage.id).subscribe(() => {
            abp.notify.success(this.l('SuccessfullyDeleted'));
            this.router.navigate([this.route.url]);
            // this.GetMessages();
          });
        }
      }
    );
  }
  private showReplyEmailMessageDialog(id?: number): void {
    let replyEmailMessageDialog: BsModalRef;
    replyEmailMessageDialog = this._modalService.show(
      ReplyEmailMessageDialogComponent,
      {
        backdrop: 'static',
        keyboard: false,
        animated: true,
        ignoreBackdropClick: true,
        class: 'modal-lg',
        initialState: {
          id: id,
        },
      }
    );
    replyEmailMessageDialog.content.onSave.subscribe(() => {
      // this.router.navigate([this.route.url]);
      this.GetMessages();
    });
  }

  private showCreateOrForwardEmailMessageDialog(id?: number): void {
    let createOrForwardEmailMessageDialog: BsModalRef;
    if (!id) {
      createOrForwardEmailMessageDialog = this._modalService.show(
        CreateEmailMessageDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
        }
      );
    } else {
      createOrForwardEmailMessageDialog = this._modalService.show(
        ForwardEmailMessageDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
          initialState: {
            id: id,
          },
        }
      );
    }

    createOrForwardEmailMessageDialog.content.onSave.subscribe(() => {
      // this.router.navigate(['email-messages']);
      this.GetMessages();
    });
  }
}
