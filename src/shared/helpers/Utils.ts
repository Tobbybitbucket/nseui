import { Injectable } from '@angular/core';
import * as moment from 'moment';

export class Utils {
    static getStatus() {
        return {
            unassigned: 'blue',
            'under-review': 'orange',
            approved: 'green',
            listed: 'nse',
            delisted: 'grey',
            denied: 'red',
            suspended: 'purple',
            withdrawn: 'brown',
        };
    }

    static getMonths() {
        return [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sept',
            'Oct',
            'Nov',
            'Dec'
        ];
    }
}


@Injectable()
export class UtilityService {
    constructor() {
    }

    isObjectEmpty(obj) {
        for (const prop in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                return false;
            }
        }

        return true;
    }

    isArrayEmpty(arr) {
        return !arr || arr.length === 0;
    }

    isEmpty(str) {
        return !str || 0 === str.length;
    }

    formatCharLength(str: String, len: any) {
        return str && str.length > len ? `${str.substring(0, len - 1)}...` : str;
    }

    capitalizeFirstLetter(str: string) {
        return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
    }

    formatDate(str: string, type?: string) {
        if (type === 'fullDateTime') {
            return moment(str).format('MMM DD, YYYY, h:mm a');
        }

        if (type === 'fullDate') {
            return moment(str).format('MMM DD, YYYY');
        }

        return moment(str).format('MMM DD');
    }
}
