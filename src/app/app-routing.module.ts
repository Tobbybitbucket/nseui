import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { UsersComponent } from './users/users.component';
import { TenantsComponent } from './tenants/tenants.component';
import { RolesComponent } from 'app/roles/roles.component';
import { ChangePasswordComponent } from './users/change-password/change-password.component';
import { FaqComponent } from './faq/faq.component';
import { MessagesComponent } from './messages/messages.component';
import { UserGuidesComponent } from './user-guides/user-guides.component';
import { ApplicationComponent } from './application/application.component';
import { ProfileComponent } from './profile/profile.component';
import { SettingsComponent } from './settings/settings.component';
import { EquityApplicationComponent } from './workspace/equity-application/equity-application.component';
import { StateBondsComponent } from './workspace/state-bonds/state-bonds.component';
import { CorporateBondsComponent } from './workspace/corporate-bonds/corporate-bonds.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: AppComponent,
                children: [
                    { path: 'home', component: HomeComponent, canActivate: [AppRouteGuard] },
                    { path: 'users', component: UsersComponent, data: { permission: 'Pages.Users' }, canActivate: [AppRouteGuard] },
                    { path: 'roles', component: RolesComponent, data: { permission: 'Pages.Roles' }, canActivate: [AppRouteGuard] },
                    { path: 'tenants', component: TenantsComponent, data: { permission: 'Pages.Tenants' }, canActivate: [AppRouteGuard] },
                    { path: 'about', component: AboutComponent },
                    { path: 'update-password', component: ChangePasswordComponent },
                    { path: 'faq', component: FaqComponent },
                    { path: 'messages', component: MessagesComponent },
                    { path: 'userguides', component: UserGuidesComponent },
                    { path: 'new-application', component: ApplicationComponent },
                    { path: 'equity-application', component: EquityApplicationComponent },
                    { path: 'state-bonds', component: StateBondsComponent },
                    { path: 'corporate-bonds', component: CorporateBondsComponent },
                    { path: 'profile', component: ProfileComponent },
                    { path: 'settings', component: SettingsComponent },

                ]
            }
        ])
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
