import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent {

  showNotification = false;
  showMessages = false;

  notifications = [
    {
      title: 'Notification Title',
      description: 'This is the content of the notification. This is the content of the notification.',
      isActive: true,
    },
    {
      title: 'Notification Title',
      description: 'This is the content of the notification. This is the content of the notification.',
      isActive: false,
    },
    {
      title: 'Notification Title',
      description: 'This is the content of the notification. This is the content of the notification.',
      isActive: false,
    }
  ];

  messages = [
    {
      title: 'Message Title',
      description: 'This is the content of the message. This is the content of the message.',
      isActive: true,
    },
    {
      title: 'Message Title',
      description: 'This is the content of the message. This is the content of the message.',
      isActive: false,
    },
    {
      title: 'Message Title',
      description: 'This is the content of the message. This is the content of the message.',
      isActive: false,
    }
  ];

  toggleNotificationDialog() {
    this.showNotification = !this.showNotification;
  }

  toggleMessagesDialog() {
    this.showMessages = !this.showMessages;
  }
}
